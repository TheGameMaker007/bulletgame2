﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class flashText : MonoBehaviour
{
    private Text myText;
    private string myString;
    private bool showText = true;
    // Start is called before the first frame update
    void Start()
    {
        myText = gameObject.GetComponent<Text>();
        myString = myText.text;
        Invoke("FlashText", 0.1f);
    }

   void FlashText()
    {
        if (showText)
        {
            myText.text = myString;
            showText = false;
            Invoke("FlashText", 1f);
        }
        else
        {
            myText.text = "";
            showText = true;
            Invoke("FlashText", 0.3f);
        }
    }
}
