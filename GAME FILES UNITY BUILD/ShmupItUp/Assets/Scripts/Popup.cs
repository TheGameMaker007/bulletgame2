﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : MonoBehaviour
{
    public GameObject PopupTextPrefab;


    // Start is called before the first frame update
    void Start()
    {
        if(PopupTextPrefab != null)
        {
            ShowPopupText();//Working on popup text
        }
        
    }

    void ShowPopupText()
    {
        Instantiate(PopupTextPrefab, transform.position, Quaternion.identity, transform);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
