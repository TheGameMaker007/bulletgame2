﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turretScript : MonoBehaviour
{
    private GameObject player;
    public bool flip = false;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player"); //finding player
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            if (flip)
            {
                transform.LookAt(2 * transform.position - player.transform.position);
            }
            else
            {
                transform.LookAt(player.transform);
            }
        }
        
        
        
       // transform.LookAt(2 * transform.position - player.transform.position);
    }
}
