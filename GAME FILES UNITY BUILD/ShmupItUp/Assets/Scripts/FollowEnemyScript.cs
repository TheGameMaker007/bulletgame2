﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowEnemyScript : MonoBehaviour
{
    public Transform targetToFace;
    public ParticleSystem[] ShotParticleSystems;
    public float SecondsBetweenShotSystemChanges = 5.0f;

    int CurrentShotParticleSystemIndex;
    float SecondsSinceLastSystemChange;
    public Vector3 velocity = new Vector3(1, 0, 0);

    // Use this for initialization
    void Start()
    {
        CurrentShotParticleSystemIndex = 0;
        SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

        ParticleSystem nextSystem = GetCurrentParticleSystem();
        if (nextSystem != null)
        {
            nextSystem.Play();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        GameObject player = GameObject.FindGameObjectWithTag("Player"); //finding player
        if (player != null)
        {
            targetToFace = player.transform;

            float playerZ = 0.0f, ownZ = 0.0f;

            playerZ = targetToFace.position.z;
            ownZ = transform.position.z;

            if (ownZ < playerZ)
            {
                transform.Translate(Vector3.back * Time.deltaTime); //Enemy passes Player
            }
            else
            {
                transform.LookAt(targetToFace); //face player
                transform.Translate(velocity * Time.deltaTime); //MAKE ENEMY MOVE
            }
        }
        else if (player = null)
        {
            transform.Translate(Vector3.back * Time.deltaTime);//LOGIC TO STOP GAME CRASH
        }




        
        SecondsSinceLastSystemChange -= Time.deltaTime;
        if (SecondsSinceLastSystemChange <= 0.0f)
        {
            SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

            ParticleSystem previousSystem = GetCurrentParticleSystem();
            if (previousSystem != null)
            {
                previousSystem.Stop();
            }

            CurrentShotParticleSystemIndex++;
            if (CurrentShotParticleSystemIndex >= ShotParticleSystems.Length)
            {
                CurrentShotParticleSystemIndex = 0;
            }

            ParticleSystem nextSystem = GetCurrentParticleSystem();
            if (nextSystem != null)
            {
                nextSystem.Play();
            }
        }
    }

    ParticleSystem GetCurrentParticleSystem()
    {
        if (CurrentShotParticleSystemIndex < ShotParticleSystems.Length)
        {
            return ShotParticleSystems[CurrentShotParticleSystemIndex];
        }

        return null;
    }
}
