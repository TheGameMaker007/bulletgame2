﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAreaMovement : MonoBehaviour
{
	public Vector3 MovementPerSecond;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            MovementPerSecond = new Vector3(0, 0, 10);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            MovementPerSecond = new Vector3(0, 0, 3);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            MovementPerSecond = new Vector3(0, 0, 0.5f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            MovementPerSecond = new Vector3(0, 0, 3);
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position + MovementPerSecond * Time.deltaTime;
    }
}
