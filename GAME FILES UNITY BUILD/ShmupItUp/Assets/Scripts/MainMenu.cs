﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Toggle tToggle;

    private void Start()
    {
        if (tToggle)
        {
           // tToggle.onValueChanged.AddListener(delegate { setTutorial(tToggle); });

            if (PlayerPrefs.GetInt("FirstTime", 1) == 0)
            {
                tToggle.isOn = false;
            }
            else
            {
                tToggle.isOn = true;
            }
        }
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void PlayGame(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void QuitGame ()
    {
        Debug.Log("QUIT!");
        PlayerPrefs.SetInt("SkipIntro", 0);
        Application.Quit();
    }

    public void setTutorial(Toggle foo)
    {
        if (tToggle.isOn)
        {
            PlayerPrefs.SetInt("FirstTime", 1);
        }
        else
        {
            PlayerPrefs.SetInt("FirstTime", 0);
        }
    }
}
