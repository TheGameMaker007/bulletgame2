﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShots : MonoBehaviour
{

    public GameObject Shot;
    public Transform BulletSpawn;
    public float fireRate;
    private float nextFire;
    AudioSource audioData;
    public bool CanShoot = true;

    public bool bossGun = false;
    public GameObject boss;

    public enum BossNum
    {
        Boss1,
        miniBoss1,
        Boss2,
        Boss3
    }
    public BossNum bosstype;

    // public BossEnemy boss;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CanShoot)
        {
            if (Time.time > nextFire)
            {
                nextFire = Time.time  + fireRate;
                Instantiate(Shot, BulletSpawn.position, BulletSpawn.rotation);
                audioData = GetComponent<AudioSource>();
                audioData.Play();

                //boss.Shoot();

                Debug.Log("bang");

                if (bossGun)
                {
                    playBossAnimation();
                }
            }
        }
    }

    protected virtual void playBossAnimation()
    {
        switch (bosstype)
        {
            case BossNum.Boss1:
                {
                    boss.GetComponent<Boss1>().playAnimation("RArmExtend");
                    break;
                }
            default:
                break;
        }
    }
}
