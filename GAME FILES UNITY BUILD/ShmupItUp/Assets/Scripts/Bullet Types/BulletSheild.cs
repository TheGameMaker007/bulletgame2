﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSheild : SpreadShot
{
    protected override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void fire()
    {
        if (CanShoot)
        {
            audioData = GetComponent<AudioSource>();
            audioData.Play();
            Debug.Log("bang");
            for (int i = 0; i < bullets.Count; ++i)
            {
                bullets[i] = Random.rotation;
                GameObject b = Instantiate(bullet, BarrelExit.position, BarrelExit.rotation);
                b.transform.rotation = Quaternion.RotateTowards(b.transform.rotation, bullets[i], spreadAngle);

            }
        }
    }

    protected override void resetShot()
    {
        base.resetShot();
    }
}