﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullets : MonoBehaviour
{
    public bool setFacingAtSpawn = true;
    public Transform targetToFace;

    public int maxReflectionCount = 5; //For reflecting bullets
    public float maxStepDistance = 200;

    public float speed;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        Destroy(gameObject, 10f);
        if (setFacingAtSpawn == true)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player"); //finding player
            if (player != null)
            {
                targetToFace = player.transform;
                Vector3 target = targetToFace.position;
                float scaler = Vector3.Distance(transform.position, targetToFace.position);
                float distZ = target.z - transform.position.z;
               // scaler = Mathf.Abs(distZ) / scaler;
                //scaler = distZ / 5.69f;
                Debug.Log(distZ);
                if(distZ > 1)
                {
                    //scaler = distZ / 3.3f;
                    scaler /= 8f;
                    scaler *= distZ / 3.3f;
                    target.z += 3f +  1* scaler;
                   // Debug.Log(scaler);
                }else if (distZ < -1)
                {
                    // scaler = Mathf.Abs(distZ / 5.67f);
                    scaler /= 9f;
                    scaler *= Mathf.Abs(distZ / 5.6f);
                    target.z += 1.5f + 1* scaler;
                }
                else
                {
                    scaler /= 8f;
                    scaler *= Mathf.Abs(distZ);
                    target.z += 2f + 1*  scaler;
                }
                transform.LookAt(target);
            }
        }
    }

    /*public void DrawPredictedReflectionPattern(Vector3 position, Vector3 direction, int reflectionsRemaining) //REFLECTING BULLETS
    {
        for (int i = reflectionsRemaining; i > 0; i--)
        {
            Vector3 startingPosition = position;

            Ray ray = new Ray(position, direction);
            /*RaycastHit hit;
            if (Physics.Raycast(ray, out hit, maxStepDistance))
            {
                direction = Vector3.Reflect(direction, hit.normal); //finish this, should give us new direction
                position = hit.point;
            }
            else
            {
                position += direction * maxStepDistance;
            }
        }
    }*/

    // Update is called once per frame
    protected virtual void Update()
    {
        if (setFacingAtSpawn == true)
        {
            //transform.LookAt(targetToFace);
            GetComponent<Rigidbody>().velocity = transform.forward * speed;
        }
    }

    protected virtual void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "attack")
        {

            //GrazeBar GrazeLevel = GameObject.Find("TimeManager"); Add value to grazebar
            Destroy(gameObject);
        }

        else if (collisionInfo.gameObject.tag == "Player" || collisionInfo.gameObject.tag == "PlayZone")
        {
            Destroy(gameObject);
        }
    }


    public virtual void DrawPredictedReflectionPattern(Vector3 position, Vector3 direction, int reflectionsRemaining) //REFLECTING BULLETS
    {
        if (reflectionsRemaining ==0)
        {
            return;
        }

        Vector3 startingPosition = position;

        Ray ray = new Ray(position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray,out hit, maxStepDistance))
        {
            direction = Vector3.Reflect(direction, hit.normal); //finish this, should give us new direction
            position = hit.point;
            GetComponent<Rigidbody>().velocity = Vector3.back * speed;
        }
        else
        {
            position += direction * maxStepDistance;
            GetComponent<Rigidbody>().velocity = Vector3.back * speed;
        }

        DrawPredictedReflectionPattern(position, direction, reflectionsRemaining - 1); // + direction * maxStepDistance (Insert if things go wrong)
    }
    
}
