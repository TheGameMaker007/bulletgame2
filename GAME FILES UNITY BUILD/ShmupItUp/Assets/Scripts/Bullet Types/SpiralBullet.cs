﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralBullet : RadialBullet
{
    protected float angle = 0f;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void SpawnProjectile()
    {
        if (CanShoot)
        {
            int _numberOfProjectiles = numberOfProjectiles;
            float angleStep = 360f / _numberOfProjectiles;

            audioData = GetComponent<AudioSource>();
            audioData.Play();
            Debug.Log("bang");

            // Direction Calculations
            float projectileDirXPosition = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
            float projectileDirYPosition = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;

            Vector3 projectileVector = new Vector3(projectileDirXPosition, projectileDirYPosition, 0);
            Vector3 projectileMoveDirection = (projectileVector - startPoint).normalized * projectileSpeed;

            GameObject tmpObj = Instantiate(ProjectilePrefab, startPoint, Quaternion.identity);
            tmpObj.GetComponent<Rigidbody>().velocity = new Vector3(projectileMoveDirection.x, 0, projectileMoveDirection.y);

            angle += angleStep;
        }
    }
}
