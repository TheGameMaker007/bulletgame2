﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialBullet : MonoBehaviour
{
    [Header("Projectile Settings")]
    public int numberOfProjectiles;
    public float projectileSpeed;
    public float fireRate;
    protected float nextFire;
    public GameObject ProjectilePrefab;
    protected AudioSource audioData;
    public bool CanShoot;

    [Header("Private Variables")]
    protected Vector3 startPoint;
    protected const float radius = 1f;

    public bool bossGun = false;
    public GameObject boss;

    public enum BossNum
    {
        Boss1,
        miniBoss1,
        Boss2,
        Boss3
    }
    public BossNum bosstype;


    protected virtual void Start()
    {
        CanShoot = true;   
        InvokeRepeating("SpawnProjectile", fireRate/2, fireRate);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //if (Time.time > nextFire)
        //{
        //nextFire += fireRate;
        startPoint = transform.position;
        //SpawnProjectile(numberOfProjectiles);

        //}
    }

    protected virtual void SpawnProjectile()
    {
        if (CanShoot)
        {
            int _numberOfProjectiles = numberOfProjectiles;
            float angleStep = 360f / _numberOfProjectiles;
            float angle = 0f;

            audioData = GetComponent<AudioSource>();
            audioData.Play();
            Debug.Log("bang");

            if (bossGun)
            {
                playBossAnimation();
            }

            for (int i = 0; i <= _numberOfProjectiles - 1; i++)
            {
                // Direction Calculations
                float projectileDirXPosition = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
                float projectileDirYPosition = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;

                Vector3 projectileVector = new Vector3(projectileDirXPosition, projectileDirYPosition, 0);
                Vector3 projectileMoveDirection = (projectileVector - startPoint).normalized * projectileSpeed;

                GameObject tmpObj = Instantiate(ProjectilePrefab, startPoint, Quaternion.identity);
                tmpObj.GetComponent<Rigidbody>().velocity = new Vector3(projectileMoveDirection.x, 0, projectileMoveDirection.y);

                angle += angleStep;
            }
        }
    }

    protected virtual void playBossAnimation()
    {
        switch (bosstype)
        {
            case BossNum.Boss1:
                {
                    boss.GetComponent<Boss1>().playAnimation("Burst");
                    break;
                }
            default:
                break;
        }
    }
}
