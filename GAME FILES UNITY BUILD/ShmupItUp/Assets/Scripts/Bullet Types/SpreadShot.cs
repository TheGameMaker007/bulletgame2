﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadShot : MonoBehaviour
{
    public int pelletCount;
    public float spreadAngle;
    public float bulletFireVelocity=1;
    public GameObject bullet;
    public Transform BarrelExit;
    protected float nextFire;
    public float fireRate;
    protected List<Quaternion> bullets;
    protected AudioSource audioData;
    public bool CanShoot = true;
    private bool shootBullet = true;

    public bool bossGun = false;
    public GameObject boss;

    public enum BossNum
    {
        Boss1,
        miniBoss1,
        Boss2,
        Boss3
    }
    public BossNum bosstype;

    protected virtual void Awake()
    {
        bullets = new List<Quaternion>(pelletCount);
        for (int i = 0; i < pelletCount;  i++)
        {
            bullets.Add(Quaternion.Euler(Vector3.zero));
        }
    }

    protected virtual void Start()
    {
        InvokeRepeating("fire", fireRate / 2, fireRate);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //if (Time.time > nextFire)
        //{
            nextFire += fireRate;
        //fire();

        if (!shootBullet)
        {
            Invoke("resetShot", 3f);
        }
    }

    protected virtual void fire()
    {
        if (CanShoot && shootBullet)
        {
            audioData = GetComponent<AudioSource>();
            audioData.Play();
            Debug.Log("bang");
            if (bossGun)
            {
                playBossAnimation();
            }

            for (int i = 0; i < bullets.Count; ++i)
            {
                bullets[i] = Random.rotation;
                GameObject b = Instantiate(bullet, BarrelExit.position, BarrelExit.rotation);
                b.transform.rotation = Quaternion.RotateTowards(b.transform.rotation, bullets[i], spreadAngle);
                b.GetComponent<Rigidbody>().AddForce(b.transform.right * bulletFireVelocity);

            }
            shootBullet = false;
        }
    }

    protected virtual void resetShot()
    {
        shootBullet = true;
    }

    protected virtual void playBossAnimation()
    {
        switch (bosstype)
        {
            case BossNum.Boss1:
                {
                    boss.GetComponent<Boss1>().playAnimation("Burst");
                    break;
                }
            default:
                break;
        }
    }
}
