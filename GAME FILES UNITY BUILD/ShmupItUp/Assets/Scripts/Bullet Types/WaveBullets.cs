﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveBullets : EnemyBullets
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (setFacingAtSpawn == true)
        {
            GetComponent<Rigidbody>().velocity = transform.forward * speed;
            GetComponent<Rigidbody>().velocity = transform.right + new Vector3(5, 0, 0);
            GetComponent<Rigidbody>().velocity = transform.right + new Vector3(-5, 0, 0);
        }
    }

    protected override void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "attack")
        {

            //GrazeBar GrazeLevel = GameObject.Find("TimeManager"); Add value to grazebar
            Destroy(gameObject);
        }

        else if (collisionInfo.gameObject.tag == "Player" || collisionInfo.gameObject.tag == "PlayZone")
        {
            Destroy(gameObject);
        }
    }

    public override void DrawPredictedReflectionPattern(Vector3 position, Vector3 direction, int reflectionsRemaining) //REFLECTING BULLETS
    {
        if (reflectionsRemaining == 0)
        {
            return;
        }

        Vector3 startingPosition = position;

        Ray ray = new Ray(position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, maxStepDistance))
        {
            direction = Vector3.Reflect(direction, hit.normal); //finish this, should give us new direction
            position = hit.point;
            GetComponent<Rigidbody>().velocity = Vector3.back * speed;
        }
        else
        {
            position += direction * maxStepDistance;
            GetComponent<Rigidbody>().velocity = Vector3.back * speed;
        }

        DrawPredictedReflectionPattern(position, direction, reflectionsRemaining - 1); // + direction * maxStepDistance (Insert if things go wrong)
    }
}
