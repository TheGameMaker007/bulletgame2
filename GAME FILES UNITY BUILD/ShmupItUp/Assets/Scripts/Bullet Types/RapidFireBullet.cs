﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RapidFireBullet : SpiralBullet
{
    private bool swap = false;
    protected override void Start()
    {
        angle = 180;
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void SpawnProjectile()
    {
        if (CanShoot)
        {
            int _numberOfProjectiles = numberOfProjectiles;
            float angleStep = 360f / _numberOfProjectiles;

            audioData = GetComponent<AudioSource>();
            audioData.Play();
            Debug.Log("bang");

            // Direction Calculations
            float projectileDirXPosition = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
            float projectileDirYPosition = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;

            Vector3 projectileVector = new Vector3(projectileDirXPosition, projectileDirYPosition, 0);
            Vector3 projectileMoveDirection = (projectileVector - startPoint).normalized * projectileSpeed;

            GameObject tmpObj = Instantiate(ProjectilePrefab, startPoint, Quaternion.identity);
            tmpObj.GetComponent<Rigidbody>().velocity = new Vector3(projectileMoveDirection.x, 0, projectileMoveDirection.y);

            if(angle >= 270)
            {
                angle -= angleStep;
                swap = true;
            }
            else if(angle <= 90)
            {
                angle += angleStep;
                swap = false;
            }

            if (swap)
            {
                angle -= angleStep;
            }
            else
            {
                angle += angleStep;
            }
        }
    }
}
