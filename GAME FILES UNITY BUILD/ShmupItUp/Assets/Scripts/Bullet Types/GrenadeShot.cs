﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeShot : SpreadShot
{
    private Vector3 pos;
    protected override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void fire()
    {
        if (CanShoot)
        {
            audioData = GetComponent<AudioSource>();
            audioData.Play();
            Debug.Log("bang");
            pos = BarrelExit.position;
            pos.z -= 3;
            for (int i = 0; i < bullets.Count; ++i)
            {
                bullets[i] = Random.rotation;
                //BarrelExit.position.Set(BarrelExit.position.x, BarrelExit.position.y, BarrelExit.position.z - 30);
                GameObject b = Instantiate(bullet, pos, BarrelExit.rotation);
                b.transform.rotation = Quaternion.RotateTowards(b.transform.rotation, bullets[i], spreadAngle);
                b.GetComponent<Rigidbody>().AddForce(b.transform.right * bulletFireVelocity);

            }
        }
    }
}
