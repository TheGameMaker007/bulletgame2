﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour
{
    public int EnemyHp;
    public int MaxEnemyHp = 3;
    //AudioSource AudioData;

    // Start is called before the first frame update
    void Start()
    {
        EnemyHp = MaxEnemyHp;
    }

    // Update is called once per frame
    void Update()
    {
        if (EnemyHp <= 0)
        {
            //AudioData.Play();
            Destroy(gameObject);
        }
    }
}
