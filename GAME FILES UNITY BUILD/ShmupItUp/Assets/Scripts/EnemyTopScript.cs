﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTopScript : MonoBehaviour
{

    public ParticleSystem[] ShotParticleSystems;
    protected float SecondsBetweenShotSystemChanges = 5.0f;

    protected int CurrentShotParticleSystemIndex;
    protected float SecondsSinceLastSystemChange;

    public Transform[] Waypoints;
    public float Speed;
    public int curWaypoint;
    public bool Patrol = true;
    public Vector3 Target;
    public Vector3 MoveDirection;
    public Vector3 Velocity;

    protected float SecondsinCurrentState;

    // Use this for initialization
    protected virtual void Start()
    {
        SecondsinCurrentState = 0;
        CurrentShotParticleSystemIndex = 0;
        SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

        ParticleSystem nextSystem = GetCurrentParticleSystem();
        if (nextSystem != null)
        {
            nextSystem.Play();
        }
    }

    // Update is called once per frame
    protected virtual void FixedUpdate()
    {

        SecondsinCurrentState += Time.deltaTime;

        if (SecondsinCurrentState > 15f) //After 15 seconds, it kills self
        {
            Destroy(gameObject);
        }

        //CREATE CODE FOR MOVEMENT HERE
        if (curWaypoint < Waypoints.Length)
        {
            Target = Waypoints[curWaypoint].position;
            MoveDirection = Target - transform.position;
            Velocity = GetComponent<Rigidbody>().velocity;

            if (MoveDirection.magnitude <1)
            {
                curWaypoint++;
            }
            else
            {
                Velocity = MoveDirection.normalized * Speed;
            }
        }
        else
        {
            if(Patrol)
            {
                curWaypoint = 0;
            }
            else
            {
                Velocity = Vector3.zero;
            }
        }
        GetComponent<Rigidbody>().velocity = Velocity;
        
        SecondsSinceLastSystemChange -= Time.deltaTime;
        if (SecondsSinceLastSystemChange <= 0.0f)
        {
            SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

            ParticleSystem previousSystem = GetCurrentParticleSystem();
            if (previousSystem != null)
            {
                previousSystem.Stop();
            }

            CurrentShotParticleSystemIndex++;
            if (CurrentShotParticleSystemIndex >= ShotParticleSystems.Length)
            {
                CurrentShotParticleSystemIndex = 0;
            }

            ParticleSystem nextSystem = GetCurrentParticleSystem();
            if (nextSystem != null)
            {
                nextSystem.Play();
            }
        }
    }

    ParticleSystem GetCurrentParticleSystem()
    {
        if (CurrentShotParticleSystemIndex < ShotParticleSystems.Length)
        {
            return ShotParticleSystems[CurrentShotParticleSystemIndex];
        }
        return null;
    }
}