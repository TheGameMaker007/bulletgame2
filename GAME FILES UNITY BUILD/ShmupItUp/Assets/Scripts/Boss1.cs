﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss1 : BossEnemy
{


    void Start()
    {
        // GameObject[] gameObjectArray = GameObject.FindGameObjectsWithTag("StartDio");

        //foreach(GameObject go in gameObjectArray)
        //{
        //    go.SetActive(true); //turning on dialogue
        //}
        
        transform.Rotate(new Vector3(0, 1, 0), 180);
        base.StartBoss();
        //setCurrentState(BossState.Dialog);
        bossAnimator.Play("Move");
        //transform.Rotate(new Vector3(0, 1, 0), 180);
    }

    void FixedUpdate()
    {
        base.UpdateBoss();
    }

    public override void Dialog()
    {
        BulletSpawner.SetActive(false);
        BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
        BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
        BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
    }

    public override void Patrol()
    {
        playAnimation("Move");
        Vector3 MoveDirection = Bossmovement1Waypoints[waypointIndex].transform.position - transform.position;
        if (MoveDirection.magnitude < 0.5f)
        {
            waypointIndex++;
            if (waypointIndex >= Bossmovement1Waypoints.Length)
            {
                waypointIndex = 0;
            }
        }
        targetPoint = Bossmovement1Waypoints[waypointIndex].transform.localPosition; //Follows waypoints based on PlayZone
        transform.position = Vector3.MoveTowards(transform.position, targetPoint + PlayRoot.position, moveSpeed * Time.deltaTime);

        if (SecondsinCurrentState > 10f) //After 10 seconds, It prepares
        {
            
            base.setCurrentState(BossState.PreparetoRush);
        }
    }

    public override void Prepare()
    {
        if (player != null)
        {
            bossAnimator.SetTrigger("PreRush");
            transform.LookAt(player.transform); //faces player
            BulletSpawner.SetActive(false);
            BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
            BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
            BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
        }
        if (SecondsinCurrentState > .5f) //After 2 seconds, It rushes
        {
            Temphp = EnemyHp;
            setCurrentState(BossState.Rush);
           
        }
    }

    public override void Special()
    {

        if (player != null)
        {
            playAnimation("Rush");
            targetToFace = player.transform;

            float playerZ = 0.0f, ownZ = 0.0f;

            playerZ = targetToFace.position.z;
            ownZ = transform.position.z;

            if (ownZ < playerZ)
            {
                bossAnimator.SetTrigger("RushEnd");
                setCurrentState(BossState.Patrol); //Enemy passes Player
                transform.Rotate(new Vector3(0, 1, 0), 180);
                Invoke("ResetSpawner", 3f);
            }
            else if (Damaged()) //Collision with player attackbox, send back to patrol
            {
                bossAnimator.SetTrigger("RushEnd");
                bossAnimator.Play("Stop Move");
               // bossAnimator.SetTrigger("BossHit");
                bossAnimator.SetTrigger("BossHit");
                setCurrentState(BossState.Patrol);
                transform.Rotate(new Vector3(0, 1, 0), 180);
                Invoke("ResetSpawner", 3f);
            }
            else
            {
                transform.LookAt(targetToFace.position); //face player
                                                         //transform.Translate(velocity * Time.deltaTime); //MAKE ENEMY MOVE
                transform.position = Vector3.MoveTowards(transform.position, targetToFace.position, moveSpeed * Time.deltaTime);
            }
        }
        else if (player = null)
        {
            transform.Translate(Vector3.back * Time.deltaTime);//LOGIC TO STOP GAME CRASH
            Invoke("ResetSpawner", 3f);
        }
        //transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, moveSpeed * Time.deltaTime);

        if (SecondsinCurrentState > 1.5f) //After 1.5 seconds
        {
            bossAnimator.SetTrigger("RushEnd");
            setCurrentState(BossState.Patrol);
            transform.Rotate(new Vector3(0, 1, 0), 180);
            Invoke("ResetSpawner", 3f);
        }
    }

    public override void Death()
    {
        playAnimation("Death");
        bossBar.SetActive(false);
        BossSource.Stop();
        LevelSource.Play();
        player.GetComponent<PlayerAttack>().boss = "m1"; //DELAY THIS BY A FEW SECONDS THEN LOAD NEXT SCENE
        player.GetComponent<PlayerHealthScript>().LifeCount++;
        Destroy(gameObject);
        BossDioEnd.SetActive(true);
       // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public override void playAnimation(string s)
    {
        switch (s)
        {
            case "Rush":
                {
                    bossAnimator.SetTrigger(s);
                    break;
                }
            case "Burst":
                {
                    bossAnimator.SetTrigger(s);
                    bossAnimator.Play(s);
                    break;
                }
            case "Move":
                {
                    bossAnimator.SetTrigger(s);
                    break;
                }
            case "RArmExtend": //shoot
                {
                    bossAnimator.SetTrigger(s);
                    break;
                }
            case "Death":
                {
                    bossAnimator.Play("Stop Move");
                    bossAnimator.SetTrigger("BossHit");
                    break;
                }
            default:
                break;
        }
    }
}
