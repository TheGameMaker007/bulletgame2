﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;


public class PlayerHealthScript : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform RespawnPoint;
    //SkinMeshRender[] skinMeshRenderers; Mesh Render

    public int startingHealth = 3;
    public int currentHealth;
    public int LifeCount;
    AudioSource audioData;

    bool PlayerCanHit;

    public ShakeScript HitShake;
    public float GrazeLevel;
    public Slider GrazeCountBar;
    public GameObject HeroShip;
    public GameObject Bullet;
    public float Distance_;


    public GameObject playerShield;
    

    // Start is called before the first frame update
    void Start()
    {
        PlayerCanHit = true;
        currentHealth = startingHealth;
        LifeCount = PlayerPrefs.GetInt("PlayerLives", 5); //Change to 5 for final version
        if(LifeCount <= 0)
        {
            LifeCount = 5;
        }
    }

    // Update is called once per frame
     void FixedUpdate()
     {
        if (PlayerPrefs.GetInt("PlayerLives", 5) != LifeCount)
        {
            PlayerPrefs.SetInt("PlayerLives", LifeCount);
        }
    }

    void OnTriggerStay(Collider collisionInfo)
    {
        if (collisionInfo.tag == "Enemy" || collisionInfo.tag == "Bullet")
        {
            GrazeCountBar.value += .1f * Time.deltaTime;     //CHANGE HERE FOR GRAZE VALUE PER DODGE
            ScoreScript.scoreValue += 1; //ADD VALUE TO CURRENT SCORE
                                           //GrazeCountBar();	//Change the meter
        }
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (PlayerCanHit)
        {
            Debug.Log("Hit!");
            switch (collisionInfo.gameObject.tag)   //Check what the collider's tag is
            {
                case "Enemy":   //If it's an enemy...
                    HitShake.Shake(0.25f, 0.125f);  //Shake the screen...
                    GetComponent<PlayerHealthScript>().currentHealth -= 3;  //Die();	And die.
                    break;
                case "Bullet":  //If it's a bullet...
                    HitShake.Shake(0.25f, 0.125f);  //Shake the screen...
                    GetComponent<PlayerHealthScript>().currentHealth -= 3;//Damage(startingHealth);	And hurt the player (in this case, it's hurting them based on how much health they start with, so they'll probably die)
                    break;
            }
            if (currentHealth <= 0)
            {
                Die();
                audioData = GetComponent<AudioSource>();
                audioData.Play();
                Debug.Log("bang");
            }
            if (LifeCount <= 0)
            {
                Destroy(GameObject.FindGameObjectWithTag("Player"));
                SceneManager.LoadScene("Title Menu");
            }
        }
    }
	
	/*void grazeCountBar() //if graze issues, check here
	{
        GrazeCountBar.value = GrazeLevel;
        if (GrazeCountBar.value >= 1)
        {
            GrazeLevel = 1;  
        }
	}
	*/


	void Damage(int damage)
	{
		currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Die();
            player.transform.position = RespawnPoint.transform.transform.position;
            currentHealth = startingHealth;
        }
	}
	
    void resetCollider()
    {
        //player.GetComponent<SphereCollider>().enabled = (true);
        PlayerCanHit = (true);
        playerShield.SetActive(false);
    }

	void Die() //TURNING OFF HITBOX HAPPENS HERE
	{
        PlayerCanHit = (false);
        playerShield.SetActive(true);
		//player.transform.position = RespawnPoint.transform.transform.position;
        currentHealth = startingHealth;
        LifeCount -= 1;
        PlayerPrefs.SetInt("PlayerLives", LifeCount);
        //player.GetComponent<SphereCollider>().enabled=(false);
        Invoke("resetCollider", 2.0f); //ADJUST THIS TO CHANGE HOW MANY SECONDS OF INVINCIBILITY YOU GET

    }

    // void OnCollisionEnter(Collision collisionInfo)
    // {
    // if (collisionInfo.tag == "Enemy")
    // {
    // HitShake.Shake(0.25f, 0.125f);
    // Die();
    // }
    // }

    /*
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Hit!");
        GetComponent<PlayerHealthScript>().currentHealth -= 3;
        if (HitShake != null && !HitShake.IsShaking())
        {
            HitShake.Shake(0.25f, 0.125f);
        }
        if (collision.gameObject.tag == "Bullet")
        {
            GetComponent<PlayerHealthScript>().currentHealth -= 3;
            //currentHealth -= 3;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            if (HitShake != null && !HitShake.IsShaking())
            {
                HitShake.Shake(0.25f, 0.125f);
            }
            GetComponent<PlayerHealthScript>().currentHealth -= 3;
            //currentHealth -= 3;
        }
    } */
}
