﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss3 : MiniBoss1
{
    private int specialCount = 0;
    public GameObject[] bulletSpawns;
    public GameObject bullet;
    private int bulletCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        bulletSpawns = GameObject.FindGameObjectsWithTag("FinalBulletSpawn");
        specialCount = 0;
        bulletCount = 0;
        base.StartBoss();
        //setCurrentState(BossState.Dialog);
        bossAnimator.Play("Run Cycle");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       UpdateBoss();
    }

    protected override void UpdateBoss()
    {
        bossHealthBar.Val = EnemyHp;
        setBossHPText();
        //added by Clark
        if (Bosswave && !Bosskilled)
        {
            if (!spawnMinions)
            {
                pos = bossMinionsWaves.transform.position;
                pos.z += 0.06f;
                bossMinionsWaves.transform.position = pos;
            }
        }
        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        SecondsinCurrentState += Time.deltaTime; //State Timer

        if (EnemyHp <= 0)
        {
            Death();
        }
        if (Currentstate == BossState.Dialog)
        {
            Dialog();
        }
        else if (Currentstate == BossState.Patrol) //All logic within patrol state
        {
            Patrol();
        }

        else if (Currentstate == BossState.PreparetoRush)
        {
            spawnMinions = false;
            Prepare();
        }

        else if (Currentstate == BossState.Rush)
        {
            spawnMinions = false;
            Special();
        }
        
    }

    public override void Patrol()
    {
        spawnMinions = false;
        if(player != null)
        {
            transform.LookAt(2*transform.position - player.transform.position);
        }
        if (specialCount < 4)
        {
            if (Damaged() && numHit < 3)
            {
                numHit++;
                Move();
            }
            else if (numHit < 3)
            {
                Stay();
            }
            else if (numHit >= 3 && numHit < 6)
            {
                transform.rotation = Quaternion.identity;
                //MoveBot();
                spawnMinions = false;
                setCurrentState(BossState.PreparetoRush);
            }
        }
        else if (specialCount >= 4 && specialCount <= 7)
        {
            Move();
            if(SecondsinCurrentState > 2f)
            {
                spawnMinions = false;
                setCurrentState(BossState.PreparetoRush);
            }
        }
        else
        {
            Stay();
        }

        if (SecondsinCurrentState > 5f) //After 5 seconds, It spawns
        {
            spawnMinions = true;
            if(SecondsinCurrentState > 10f)
            {
                spawnMinions = false;
                SecondsinCurrentState = 0;
            }
        }
    }

    public override void Dialog()
    {
        Debug.Log("Boss Startup");
        BulletSpawner.SetActive(false);
        BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
        BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
        BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
    }

    public override void Prepare()
    {
        if(specialCount < 2)
        {
            specialCount++;
            numBombs = 0;
            setCurrentState(BossState.Rush);
        //    BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
         //   BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
        }
        else if(specialCount == 2)
        {
            transform.LookAt(player.transform.position); //Faces Player
            if (SecondsinCurrentState > .5f) //After 2 seconds, It rushes
            {
                specialCount++;
                Temphp = EnemyHp;
                setCurrentState(BossState.Rush);
                BulletSpawner.SetActive(false);
                BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
                BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
                BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
            }
        }
        else if(specialCount == 3)
        {
            specialCount++;
            BulletSpawner.SetActive(false);
            BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
            BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
            BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
            removeBullets();
            freeze();
            setCurrentState(BossState.Rush);
        }
        else if(specialCount >= 4 && specialCount <= 6)
        {
            transform.LookAt(player.transform.position); //Faces Player
            if (SecondsinCurrentState > 0.5f)
            {
                specialCount++;
                Temphp = EnemyHp;
                setCurrentState(BossState.Rush);
                BulletSpawner.SetActive(false);
                BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
                BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
                BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
            }
        }
        else
        {
            specialCount++;
            BulletSpawner.SetActive(false);
            BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
            BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
            BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
            removeBullets();
            freeze();
            setCurrentState(BossState.Rush);
        }
    }

    public override void Special()
    {
        //specialCount++;
        switch (specialCount)
        {
            case 1:
                {
                    spawnSpiderBombs();
                    break;
                }
            case 2:
                {
                    spawnSpiderBombs();
                    break;
                }
            case 3:
                {
                    rush();
                    break;
                }
            case 4:
                {
                    TimeWalk();
                    break;
                }
            case 5:
                {
                    rush();
                    break;
                }
            case 6:
                {
                    rush();
                    break;
                }
            case 7:
                {
                    rush();
                    break;
                }
            case 8:
                {
                    TimeWalk();
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void spawnBullets()
    {
        if(bulletCount < bulletSpawns.Length)
        {
            GameObject b = bullet;
            b.transform.position = bulletSpawns[bulletCount].transform.position;
            b.GetComponent<EnemyBullets>().speed = 0;
            Instantiate(b);
            bulletCount++;
        }
    }

    void freeze()
    {
        player.GetComponent<HeroShipMovement>().enabled = false;
        player.GetComponentInChildren<Animator>().enabled = false;
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Check World");
        PlayRoot.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);

        //City
        foreach(GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);
        }

        //Train
        prefabs = GameObject.FindGameObjectsWithTag("World");
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);
        }
    }

    void unFreeze()
    {
        player.GetComponent<HeroShipMovement>().enabled = true;
        player.GetComponentInChildren<Animator>().enabled = true;
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Check World");
        PlayRoot.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 2);

        //City
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, -8);
        }

        //Train
        prefabs = GameObject.FindGameObjectsWithTag("World");
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, -1);
        }
    }

    void TimeWalk()
    {
        Move();
        spawnBullets();

        if (SecondsinCurrentState > 5f)
        {
            ResetSpawner();
            ResetBulletSpeed();
            unFreeze();
            waypointIndex = 0;
            bulletCount = 0;
            setCurrentState(BossState.Patrol);
        }
    }

    void spawnSpiderBombs()
    {
        Invoke("spawnBombs", fireRate / 2);
        MoveBot();
        if (SecondsinCurrentState % 2f == 0f)
        {
            resetBombs();
        }

        if (Damaged())
        {
            numHit++;
            //numBombs = 0; MORE BOMBS
        }
        if (numHit > 6)
        {
            numHit = 0;
            waypointIndex = 0;
            ResetSpawner();
            setCurrentState(BossState.Patrol);
            numBombs = 0;
        }
    }

    void rush()
    {
        if (player != null)
        {
            targetToFace = player.transform;

            float playerZ = 0.0f, ownZ = 0.0f;

            playerZ = targetToFace.position.z;
            ownZ = transform.position.z;

            if (ownZ < playerZ)
            {
                numHit = 0;
                waypointIndex = 0;
                setCurrentState(BossState.Patrol); //Enemy passes Player
                ResetSpawner();
                //Invoke("ResetSpawner", 3f);
            }
            else if (Damaged()) //Collision with player attackbox, send back to patrol
            {
                numHit = 0;
                waypointIndex = 0;
                setCurrentState(BossState.Patrol); //Enemy passes Player
                ResetSpawner();
                //Invoke("ResetSpawner", 3f);
            }
            else
            {
                transform.LookAt(targetToFace.position); //face player
                                                         //transform.Translate(velocity * Time.deltaTime); //MAKE ENEMY MOVE
                transform.position = Vector3.MoveTowards(transform.position, targetToFace.position, moveSpeed * Time.deltaTime);
            }
        }
        else if (player = null)
        {
            transform.Translate(Vector3.back * Time.deltaTime);//LOGIC TO STOP GAME CRASH
            ResetSpawner();
            //Invoke("ResetSpawner", 3f);
        }
        //transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, moveSpeed * Time.deltaTime);

        if (SecondsinCurrentState > 1.5f) //After 1.5 seconds
        {
            numHit = 0;
            waypointIndex = 0;
            setCurrentState(BossState.Patrol); //Enemy passes Player
            ResetSpawner();
            //Invoke("ResetSpawner", 3f);
        }
    }

    void ResetBulletSpeed()
    {

        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Bullet");

        foreach (GameObject bullet in prefabs)
        {
            bullet.GetComponent<EnemyBullets>().speed = 5;
        }
        GameObject b = bullet;
        b.GetComponent<EnemyBullets>().speed = 5;
    }

    void removeBullets()
    {
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Bullet");

        foreach (GameObject bullet in prefabs)
        {
            Destroy(bullet);
        }
    }

    public override void Death()
    {
        bossBar.SetActive(false);
        //winscreen.SetActive(true);
        //player.GetComponent<PlayerAttack>().boss = "4";
        player.GetComponent<PlayerHealthScript>().LifeCount++;
        BossSource.Stop();
        LevelSource.Play();
        ResetBulletSpeed();
        Destroy(gameObject);
        //SceneManager.LoadScene(4);
        BossDioEnd.SetActive(true);
    }

    public override void playAnimation(string s)
    {
        /*
        switch (s)
        {
            case "Idle":
                {
                    bossAnimator.SetTrigger(s);
                    break;
                }
            case "Charge":
                {
                    bossAnimator.ResetTrigger("Idle");
                    bossAnimator.ResetTrigger("Slash");
                    bossAnimator.ResetTrigger("Attack");
                    bossAnimator.SetTrigger(s);
                    //bossAnimator.Play("Start charging Attack");
                    break;
                }
            case "Slash":
                {
                    bossAnimator.ResetTrigger("Idle");
                    bossAnimator.ResetTrigger("Charge");
                    bossAnimator.ResetTrigger("Attack");
                    bossAnimator.SetTrigger(s);
                    //  bossAnimator.Play("Slash Arm");
                    break;
                }
            case "Attack": //shoot
                {
                    bossAnimator.ResetTrigger("Idle");
                    bossAnimator.ResetTrigger("Charge");
                    bossAnimator.ResetTrigger("Slash");
                    bossAnimator.SetTrigger(s);
                    //  bossAnimator.Play("Fire (Recoil)");
                    break;
                }
            case "Death":
                {
                    bossAnimator.Play("Death");
                    break;
                }
            default:
                break;
        }
        */
    }
}
