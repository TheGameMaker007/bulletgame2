﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueBox : MonoBehaviour
{
    public GameObject[] boxes;
    private int currbox = 0;
    private float randTime = 0.1f;

    public GameObject player;
    public GameObject trainSpawner;
    public Transform PlayRoot;
    public AudioSource levelaudio;

    // Start is called before the first frame update
    void Awake()
    {
        //PauseMenu.GameIsPaused = true;
        // Time.timeScale = 0.01f;  //pausing ruins invoke we must stop the speed of train and player here
        boxes[currbox].SetActive(true);
        // currbox++;
        levelaudio.volume *= 0.5f;
        Invoke("AnimateBoxes", 0.1f);
        //freeze();
        Invoke("freeze", 0f); //waits for next update so everything in game can load properly
    }

    // Update is called once per frame

    void AnimateBoxes()
    { 
        if(currbox < boxes.Length-1)
        {
            boxes[currbox].SetActive(false);
            currbox++;
            boxes[currbox].SetActive(true);
            randTime = Random.Range(0.1f, 0.18f);
            Invoke("AnimateBoxes", randTime);
        }
    }

    void freeze()
    {
        trainSpawner.SetActive(false);
        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        PlayRoot = GameObject.FindGameObjectWithTag("PlayZone").transform; //calls playroo
        player.GetComponent<HeroShipMovement>().enabled = false;
        player.GetComponentInChildren<Animator>().enabled = false;
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Check World");
        PlayRoot.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);

        //City
        foreach (GameObject cityPrefab in prefabs)
        {
            if(cityPrefab.GetComponent<PlayAreaMovement>() !=null)
                cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);
        }

        //Train
        prefabs = GameObject.FindGameObjectsWithTag("World");
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);
        }
    }

    void unFreeze()
    {
        player.GetComponent<HeroShipMovement>().enabled = true;
        player.GetComponentInChildren<Animator>().enabled = true;
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Check World");
        PlayRoot.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 2);

        //City
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, -8);
        }

        //Train
        prefabs = GameObject.FindGameObjectsWithTag("World");
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, -1);
        }
    }
}
