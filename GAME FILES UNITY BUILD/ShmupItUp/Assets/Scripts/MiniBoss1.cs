﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Bomber miniBoss
public class MiniBoss1 : BossEnemy
{
    public Transform[] spawnPoints;
    public GameObject Bomb;
    public int maxBombs = 7;
    public float fireRate = 1f;

    protected int numHit = 0;
    public int numBombs = 0;


    //public List<GameObject> bombs;

    // Start is called before the first frame update
    void Start()
    {
        base.StartBoss();
        bossAnimator.Play("Idle to Run");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateBoss();
    }

    protected override void StartBoss()
    {
        base.StartBoss();
        //bossAnimator.Play("Idle to Run");
    }

    protected override void UpdateBoss()
    {
        bossHealthBar.Val = EnemyHp;
        setBossHPText();

        //added by Clark
        if (Bosswave && !Bosskilled)
        {
            pos = waves.transform.position;
            pos.z += 0.045f;
            waves.transform.position = pos;
        }

        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        SecondsinCurrentState += Time.deltaTime; //State Timer

        if (EnemyHp <= 0)
        {
            Death();
        }

        if (Currentstate == BossState.Patrol) //All logic within patrol state
        {
            Patrol();
        }

        else if (Currentstate == BossState.PreparetoRush)
        {
            Prepare();
        }

        else if (Currentstate == BossState.Rush)
        {
            Special();
        }
    }

    public override void Patrol()
    {
        playAnimation("Run");
        if (player != null)
        {
            transform.LookAt(player.transform);
        }
        if (Damaged() && numHit < 3)
        {
            numHit++;
            Move();
        }
        else if (numHit < 3)
        {
            Stay();
        }
        else if (numHit >= 3 && numHit < 6)
        {
            transform.rotation = Quaternion.identity;
            MoveBot();

           // playAnimation("Jump");
           // Invoke("land", 0.5f);
            // transform.Rotate(new Vector3(0, 1, 0), 180);
            base.setCurrentState(BossState.PreparetoRush);
           // numBombs = 0;
        }
       // else if (numHit >= 6)
      //  {
       //     Move();
            //numBombs = 0;
         //   numHit = 0;
       // }
    }

    protected void Move()
    {
        //transform.LookAt(player.transform);
        Vector3 MoveDirection = Bossmovement1Waypoints[waypointIndex].transform.position - transform.position;
        if (MoveDirection.magnitude < 0.5f)
        {
            waypointIndex++;
            if (waypointIndex >= Bossmovement1Waypoints.Length - 1)
            {
                waypointIndex = 0;
            }
        }
        targetPoint = Bossmovement1Waypoints[waypointIndex].transform.localPosition; //Follows waypoints based on PlayZone
        transform.position = Vector3.MoveTowards(transform.position, targetPoint + PlayRoot.position, moveSpeed * Time.deltaTime);
        //base.setCurrentState(BossState.Patrol);
    }

    protected void MoveBot()
    {
        Vector3 MoveDirection = Bossmovement1Waypoints[waypointIndex].transform.position - transform.position;
        waypointIndex = 2;
        targetPoint = Bossmovement1Waypoints[waypointIndex].transform.localPosition; //Follows waypoints based on PlayZone
        transform.position = Vector3.MoveTowards(transform.position, targetPoint + PlayRoot.position, moveSpeed * Time.deltaTime);
        //base.setCurrentState(BossState.Patrol);
    }

    void land()
    {
        playAnimation("Land");
    }

    protected void spawnBombs()
    {
        playAnimation("Bomb");
        if(numBombs < maxBombs)
        {
            int spawn = Random.Range(0, 7);

            GameObject b = Bomb;
            //assign values to bomb script if needed here b.getComponent<blah>()
            b.transform.position = spawnPoints[spawn].position;
            b.GetComponent<SpiderEnemyScript>().Boss = gameObject;
            Instantiate(b);
            //b.GetComponent<SpiderEnemyScript>().Boss = gameObject;
           // bombs.Add(b);

            numBombs++;
        }

        //Invoke("resetBombs", fireRate);
    }

    protected void Stay()
    {
        targetPoint = Bossmovement1Waypoints[waypointIndex].transform.localPosition; //Follows waypoints based on PlayZone
        transform.position = Vector3.MoveTowards(transform.position, targetPoint + PlayRoot.position, moveSpeed * Time.deltaTime);
    }

    public override void Prepare()
    {
        // transform.LookAt(player.transform.position); //Faces Player
        // if (SecondsinCurrentState > .5f) //After 2 seconds, It rushes
        //{
        numBombs = 0;
        base.setCurrentState(BossState.Rush);
           // BulletSpawner.SetActive(false);
            BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
           // BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
            BulletSpawner.GetComponent<SpreadShot>().CanShoot = false;
       // }
    }

    protected void resetBombs()
    {
        numBombs = 0;
    }

    public override void Special()
    {
        Invoke("spawnBombs", fireRate/2);
        MoveBot();
        if(SecondsinCurrentState % 2f  == 0f)
        {
            resetBombs();
        }

        if (Damaged())
        {
            numHit++;
            //numBombs = 0;
        }
        if(numHit > 6)
        {
            numHit = 0;
            waypointIndex = 0;
            base.ResetSpawner();
            base.setCurrentState(BossState.Patrol);
          //  playAnimation("Jump");
          //  Invoke("land", 0.5f);
        }
    }

    public override void Death()
    {
        playAnimation("Trip");
        bossBar.SetActive(false);
        BossSource.Stop();
        LevelSource.Play();
        player.GetComponent<PlayerHealthScript>().LifeCount++;
        player.GetComponent<PlayerAttack>().boss = "2";
        Destroy(gameObject);
    }

    public override void playAnimation(string s)
    {
        switch (s)
        {
            case "Run":
                {
                    bossAnimator.SetTrigger(s);
                    break;
                }
            case "Jump":
                {
                    bossAnimator.ResetTrigger("Run");
                    bossAnimator.ResetTrigger("Bomb");
                    bossAnimator.SetTrigger(s);
                    bossAnimator.Play("Jump up");
                    break;
                }
            case "Land":
                {
                    bossAnimator.ResetTrigger("Jump");
                    bossAnimator.SetTrigger(s);
                    //bossAnimator.Play("Land");
                    break;
                }
            case "Bomb": //shoot
                {
                    bossAnimator.ResetTrigger("Land");
                    bossAnimator.SetTrigger(s);
                    break;
                }
            case "Trip":
                {
                    bossAnimator.Play("Trip");
                    break;
                }
            default:
                break;
        }
    }
}
