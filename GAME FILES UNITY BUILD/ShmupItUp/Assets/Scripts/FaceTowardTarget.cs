﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceTowardTarget : MonoBehaviour {

    public Transform targetToFace;

	// Use this for initialization
	void Start () {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(player != null)
        {
            targetToFace = player.transform;
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(targetToFace);
	}
}
