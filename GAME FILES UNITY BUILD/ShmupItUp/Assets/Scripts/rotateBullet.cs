﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateBullet : MonoBehaviour
{
    private float deltaAngle;
    // Start is called before the first frame update
    void Start()
    {
        deltaAngle = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        deltaAngle += 0.4f;
        transform.Rotate(new Vector3(0, 0, 1), deltaAngle);
    }
}
