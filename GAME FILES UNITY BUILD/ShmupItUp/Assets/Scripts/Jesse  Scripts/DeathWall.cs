﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWall : MonoBehaviour
{
    //Assign prefabs to spawn here
    public GameObject myPrefab = null;
    public GameObject myPrefab2 = null;
    public GameObject myPrefab3 = null;
    public GameObject myPrefab4 = null;



    // Start is called before the first frame update
    void Start()
    {
        //Creates variable assigned to world spawner
        //Creates variable assigned to train spawner
        var gObj = GameObject.Find("World Spawner");
        var gObj2 = GameObject.Find("Train Spawner");

        if (gObj)
        {
            //Gets position of world spawner
            var pos = gObj.transform.position;
            Instantiate(myPrefab, gObj.transform.position, transform.rotation * Quaternion.Euler(0f, 0f, 0f));
        }

        if (gObj2)
        {
            //Gets position of train spawner
            var pos = gObj2.transform.position;
            Instantiate(myPrefab2, gObj2.transform.position, transform.rotation * Quaternion.Euler(0f, 0f, 0f));
        }


        //creates version of prefab on start up
       // Instantiate(myPrefab, gObj.transform.position, transform.rotation * Quaternion.Euler(0f, 0f, 0f));
       // Instantiate(myPrefab2, gObj2.transform.position, transform.rotation * Quaternion.Euler(0f, 0f, 0f));

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        var gObj = GameObject.Find("World Spawner");
        var gObj2 = GameObject.Find("Train Spawner");

        if (gObj)
        {
            //Gets position of world spawner
            var pos = gObj.transform.position;
        }

        if (gObj2)
        {
            //Gets position of train spawner
            var pos = gObj2.transform.position;
        }

        if (other.gameObject.tag == "Check World")
        {
            Instantiate(myPrefab4, gObj.transform.position, transform.rotation * Quaternion.Euler(0f, 0f, 0f));
        }

        if (other.gameObject.tag == "Check Train")
        {
            Instantiate(myPrefab2, gObj2.transform.position, transform.rotation * Quaternion.Euler(0f, 0f, 0f));
        }

        //Destroying Everything
        if (other.gameObject.tag != "Check World" || other.gameObject.tag != "Check Train")
        {
            Destroy(other.gameObject);
        }
        //Deleting Enemies
        if (other.gameObject.tag == "Enemy")
        {
            EnemyHP thisEnemyHP = other.gameObject.GetComponent<EnemyHP>();
            thisEnemyHP.EnemyHp -= 3;
        }
        

    }
}