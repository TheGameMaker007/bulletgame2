﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public AudioSource LevelSource; //Audio
    public GameObject BossDioStart;
    public GameObject BossDioEnd;
	bool ShouldSpawn = false;
	public GameObject Player;
    public Transform[] WaypointList;
    //public GameObject Foe;
    //public GameObject Foe2;
    public Transform SpawnPoint_Left;
    public Transform SpawnPoint_TopLeft;
    public Transform SpawnPoint_Top;
    public Transform SpawnPoint_TopRight;
    public Transform SpawnPoint_Right;
    public Transform waypoint1;
    public Transform waypoint2;
    public Transform waypoint3;
    public Transform waypoint4;
    public Transform waypoint5;
    public Transform waypoint6;
    public Transform waypoint7;
    public Transform waypoint8;
    public Transform waypoint9;
    public Transform waypoint10;
    public Transform waypoint11;
    public Transform waypoint12;
    public Transform waypoint13;
    public Transform waypoint14;
    public Transform waypoint15;
    public Transform waypointBL;
    public Transform waypointBR;
    public Transform waypointTL;
    public Transform waypointTR;

    public Transform[] MovementList_swoopInRight;
    public Transform[] MovementList_swoopInLeft;
    public Transform[] MovementList_downMiddleRight;
    public Transform[] MovementList_downMiddleLeft;
    public Transform[] MovementList_Loops;
    public Transform[] MovementList_Stall;
    public Transform[] MovementList_Rush;
    public Transform[] MovementList_StayAtTop;

    //added by Clark
    [SerializeField]
    public GameObject Waves;
    public bool BossWave;
    public Text bossName;
    public Text bossHitText;
    public GameObject bossBar; //boss ui
    public BarScript bossHealthBar;
    public GameObject winScreen;

    public GameObject bossMinionsWaves;
    /*
    public enum WaypointList
    {
        waypoint1,
        waypoint2,
        waypoint3,
        waypoint4,
        waypoint5,
        waypoint6,
        waypoint7,
        waypoint8,
        waypoint9,
        waypoint10,
        waypoint11,
        waypoint12,
        waypoint13,
        waypoint14,
        waypoint15,
        waypointBL,
        waypointBR,
        waypointTL,
        waypointTR
    }*/

    //public GameObject[] FoesToSpawn;

    //use enum to determine spawn locations
    public enum SpawnPoints
    {
        left,
        topleft,
        top,
        topright,
        right
    }

    public enum MovementPatterns
    {
        swoopInRight,
        swoopInLeft,
        downMiddleRight,
        downMiddleLeft,
        Loops,
        Stall,
        Rush,
        StayAtTop
    }

    public enum BossNum
    {
        Boss1,
        miniBoss1,
        Boss2,
        Boss3
    }
    public BossNum boss;
    //public SpawnPoints spawnLocation;
    //public SpawnPoints spawnLocation1;
    //public SpawnPoints spawnLocation2;
    //public SpawnPoints[] spawnLocations;

    [System.Serializable]
    public struct SpawnEntry
    {
        public GameObject FoePrototype;
        public SpawnPoints SpawnPoint;
        public MovementPatterns Movement;
    }
    public SpawnEntry[] spawnEvents;

    // Start is called before the first frame update
    void Start()
    {
        //added by clark
        if (BossWave)
        {
            setBoss();
        }
    }

    void SpawnFoe(Vector3 SpawnPosition, SpawnEntry currSpawn) //New Function, set parameters in parenthesis
    {
        GameObject foe = Instantiate(currSpawn.FoePrototype, SpawnPosition, currSpawn.FoePrototype.transform.rotation);
        //Set Spawn locations
        Debug.Log("SPAWN!");

        //GameObject foe = Instantiate(currSpawn.FoePrototype, SpawnPosition, currSpawn.FoePrototype.transform.rotation);
       // GameObject foe = currSpawn.FoePrototype;
      //  foe.transform.position = SpawnPosition;
       // foe.transform.rotation = foe.transform.rotation;
        //Instantiate(foe);

        //GameObject foe2 = Instantiate(Foe, SpawnPoint_Left.position, Quaternion.identity);

        EnemyTopScript enemyTop2 = foe.GetComponent<EnemyTopScript>();
        if (enemyTop2 != null)
        {
            
            if (currSpawn.Movement == MovementPatterns.swoopInRight) //START OF MOVEMENT PATTERN STORAGE
            {
                WaypointList = MovementList_swoopInRight;
                
            }

            if (currSpawn.Movement == MovementPatterns.swoopInLeft)
            {
                WaypointList = MovementList_swoopInLeft;
                
            }

            if (currSpawn.Movement == MovementPatterns.downMiddleRight)
            {
                WaypointList = MovementList_downMiddleRight;
                
            }

            if (currSpawn.Movement == MovementPatterns.downMiddleLeft)
            {
                WaypointList = MovementList_downMiddleLeft;
                
            }

            if (currSpawn.Movement == MovementPatterns.Loops)
            {
                WaypointList = MovementList_Loops;
                
            }

            if (currSpawn.Movement == MovementPatterns.Stall)
            {
                WaypointList = MovementList_Stall;
                
            }

            if (currSpawn.Movement == MovementPatterns.StayAtTop)
            {
                WaypointList = MovementList_StayAtTop;
                
            }

            if (currSpawn.Movement == MovementPatterns.Rush)
            {
                WaypointList = MovementList_Rush;
            }

            enemyTop2.Waypoints = WaypointList;    //WaypointList would be the list of waypoints you have in EnemyTopScript

        }

        FaceTowardTarget faceToward = foe.GetComponentInChildren<FaceTowardTarget>();
        if (faceToward != null)
        {
            faceToward.targetToFace = Player.transform;
        }

        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(ShouldSpawn)
		{
            foreach(SpawnEntry currSpawn in spawnEvents)
            {

                if (currSpawn.SpawnPoint == SpawnPoints.left)
                {

                    SpawnFoe(SpawnPoint_Left.position, currSpawn);

                }

                if (currSpawn.SpawnPoint == SpawnPoints.topleft)
                {
                    SpawnFoe(SpawnPoint_TopLeft.position, currSpawn);

                }

                if (currSpawn.SpawnPoint == SpawnPoints.top)
                {
                    SpawnFoe(SpawnPoint_Top.position, currSpawn);

                }

                if (currSpawn.SpawnPoint == SpawnPoints.topright)
                {
                    SpawnFoe(SpawnPoint_TopRight.position, currSpawn);

                }

                if (currSpawn.SpawnPoint == SpawnPoints.right)
                {
                    SpawnFoe(SpawnPoint_Right.position, currSpawn);

                }
            }
        }
    }
	
	private void OnTriggerEnter(Collider other)
    {
		Debug.Log("I have been entered!");
		ShouldSpawn = true;
	}

    public void setBoss()
    {
        SpawnEntry currspawn = spawnEvents[0];
        switch (boss)
        {
            case BossNum.Boss1:
                {
                    currspawn.FoePrototype.GetComponent<Boss1>().waves = Waves;
                    currspawn.FoePrototype.GetComponent<Boss1>().bossHealthBar = bossHealthBar;
                    currspawn.FoePrototype.GetComponent<Boss1>().bossHitText = bossHitText;
                    currspawn.FoePrototype.GetComponent<Boss1>().bossBar = bossBar;
                    currspawn.FoePrototype.GetComponent<Boss1>().bossNametext = bossName;
                    currspawn.FoePrototype.GetComponent<Boss1>().winscreen = winScreen;
                    currspawn.FoePrototype.GetComponent<Boss1>().LevelSource = LevelSource;
                    currspawn.FoePrototype.GetComponent<Boss1>().BossDioStart = BossDioStart;
                    currspawn.FoePrototype.GetComponent<Boss1>().BossDioEnd = BossDioEnd;
                    break;
                }
            case BossNum.miniBoss1:
                {
                    currspawn.FoePrototype.GetComponent<MiniBoss1>().waves = Waves;
                    currspawn.FoePrototype.GetComponent<MiniBoss1>().bossHealthBar = bossHealthBar;
                    currspawn.FoePrototype.GetComponent<MiniBoss1>().bossHitText = bossHitText;
                    currspawn.FoePrototype.GetComponent<MiniBoss1>().bossBar = bossBar;
                    currspawn.FoePrototype.GetComponent<MiniBoss1>().bossNametext = bossName;
                    currspawn.FoePrototype.GetComponent<MiniBoss1>().winscreen = winScreen;
                    currspawn.FoePrototype.GetComponent<MiniBoss1>().LevelSource = LevelSource;
                    break;
                }
            case BossNum.Boss2:
                {
                    currspawn.FoePrototype.GetComponent<Boss2>().waves = Waves;
                    currspawn.FoePrototype.GetComponent<Boss2>().bossHealthBar = bossHealthBar;
                    currspawn.FoePrototype.GetComponent<Boss2>().bossHitText = bossHitText;
                    currspawn.FoePrototype.GetComponent<Boss2>().bossBar = bossBar;
                    currspawn.FoePrototype.GetComponent<Boss2>().bossNametext = bossName;
                    currspawn.FoePrototype.GetComponent<Boss2>().BossDioStart = BossDioStart;
                    currspawn.FoePrototype.GetComponent<Boss2>().BossDioEnd = BossDioEnd;
                    if (bossMinionsWaves)
                    {
                        currspawn.FoePrototype.GetComponent<Boss2>().bossMinionsWaves = bossMinionsWaves;
                    }
                    currspawn.FoePrototype.GetComponent<Boss2>().winscreen = winScreen;
                    currspawn.FoePrototype.GetComponent<Boss2>().LevelSource = LevelSource;
                    break;
                }
            case BossNum.Boss3:
                {
                    currspawn.FoePrototype.GetComponent<Boss3>().waves = Waves;
                    currspawn.FoePrototype.GetComponent<Boss3>().bossHealthBar = bossHealthBar;
                    currspawn.FoePrototype.GetComponent<Boss3>().bossHitText = bossHitText;
                    currspawn.FoePrototype.GetComponent<Boss3>().bossBar = bossBar;
                    currspawn.FoePrototype.GetComponent<Boss3>().bossNametext = bossName;
                    currspawn.FoePrototype.GetComponent<Boss3>().BossDioStart = BossDioStart;
                    currspawn.FoePrototype.GetComponent<Boss3>().BossDioEnd = BossDioEnd;
                    if (bossMinionsWaves)
                    {
                        currspawn.FoePrototype.GetComponent<Boss3>().bossMinionsWaves = bossMinionsWaves;
                    }
                    currspawn.FoePrototype.GetComponent<Boss3>().winscreen = winScreen;
                    currspawn.FoePrototype.GetComponent<Boss3>().LevelSource = LevelSource;
                    break;
                }
            default:
                {
                    currspawn.FoePrototype.GetComponent<BossEnemy>().waves = Waves;
                    currspawn.FoePrototype.GetComponent<BossEnemy>().bossHealthBar = bossHealthBar;
                    currspawn.FoePrototype.GetComponent<BossEnemy>().bossHitText = bossHitText;
                    currspawn.FoePrototype.GetComponent<BossEnemy>().bossBar = bossBar;
                    currspawn.FoePrototype.GetComponent<BossEnemy>().bossNametext = bossName;
                    if (bossMinionsWaves)
                    {
                        currspawn.FoePrototype.GetComponent<BossEnemy>().bossMinionsWaves = bossMinionsWaves;
                    }
                    currspawn.FoePrototype.GetComponent<BossEnemy>().winscreen = winScreen;
                    currspawn.FoePrototype.GetComponent<BossEnemy>().LevelSource = LevelSource;
                    break;
                }
        }
    }
}
