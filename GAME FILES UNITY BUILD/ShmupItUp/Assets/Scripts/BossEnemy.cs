﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BossEnemy : MonoBehaviour
{
    [SerializeField]
    Transform[] waypoints;

    [SerializeField]
    public float moveSpeed = 2f;

    public AudioSource LevelSource, BossSource;

    public GameObject BossDioStart; //DIALOGUE
    public GameObject BossDioEnd;

    protected int waypointIndex = 0;

    public Animator bossAnimator;

    public int EnemyHp;
    public int MaxEnemyHp = 30;
    protected int Temphp;
    public GameObject bossBar;
    public BarScript bossHealthBar;
    public string bossName;
    public Text bossNametext;
    public Text bossHitText;

    public ParticleSystem[] ShotParticleSystems;
    public float SecondsBetweenShotSystemChanges = 5.0f;

    public GameObject player;
    public Vector3 targetPoint;

    public Transform targetToFace; //Finding player
    public Vector3 velocity = new Vector3(4, 4, 4);

    public GameObject BulletSpawner;

    public string[] Bossmovement1; //Set up list initially

    protected Transform[] Bossmovement1Waypoints; //Referenced in play. Make BossMovement2Waypoints for other logic

    protected Transform PlayRoot;

    public GameObject winscreen;
    public bool lastBoss;

    //added by Clark
    [SerializeField]
    public bool Bosswave;
    public bool Bosskilled;
    public GameObject waves;
    protected Vector3 pos;

    int CurrentShotParticleSystemIndex;
    float SecondsSinceLastSystemChange;

    public GameObject bossMinionsWaves;

    public bool spawnMinions;

    public enum BossState
    {
        none,
        Dialog,
        Patrol,
        PreparetoRush,
        Rush,
    }

    public BossState Currentstate;
    protected float SecondsinCurrentState;

    public virtual void setCurrentState(BossState newState)
    {
        if (newState != Currentstate)
        {
            //When you leave current state, runs logic once before leaving state. (Particle Effects? SFX?)
            Currentstate = newState;
            //New State entered, turn on effects?
            SecondsinCurrentState = 0;
        }
    }

    // Use this for initialization
    protected virtual void StartBoss()
    {
        BossSource = GetComponent<AudioSource>();

        EnemyHp = MaxEnemyHp;
        Temphp = EnemyHp;

        bossBar.SetActive(true);
        bossHealthBar.Val = EnemyHp;
        bossHealthBar.MaxVal = MaxEnemyHp;
        setBossHPText();

        bossNametext.text = bossName;

        LevelSource.Stop();
        BossSource.Play();

        CurrentShotParticleSystemIndex = 0;
        SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

        ParticleSystem nextSystem = GetCurrentParticleSystem();
        if (nextSystem != null)
        {
            nextSystem.Play();
        }

        Bossmovement1Waypoints = new Transform[Bossmovement1.Length];
        for (int i = 0; i < Bossmovement1.Length; i++)
        {
            Bossmovement1Waypoints[i] = GameObject.Find("waypoint" + Bossmovement1[i]).transform; //Sets up boss movement pattern
        }

        PlayRoot = GameObject.FindGameObjectWithTag("PlayZone").transform; //calls playroot

        if(BossDioStart != null)
        {
            setCurrentState(BossState.Dialog);
            BossDioStart.SetActive(true);
            BossSource.volume *= 0.5f;
        }
        else
        {
            setCurrentState(BossState.Patrol); //Initial State
        }
    }
    
    public void setBossHPText()
    {
        //int htd = Mathf.CeilToInt(EnemyHp / 3);
        int rmd = 0;
        if(EnemyHp % 3 > 0)
        {
            rmd = 1;
        }
        int htd = (EnemyHp / 3) + rmd;
        bossHitText.text = htd.ToString();
    }


    public List<Material> emissionMats;
    public bool Damaged()
    {
        if (Temphp > EnemyHp)
        {
            Temphp = EnemyHp;

            Renderer[] rens = gameObject.GetComponentsInChildren<Renderer>();
            int i = 0;
            if(rens.Length >= 1)
            {
                foreach(Renderer r in rens)
                {
                    if (r.materials != null)
                    {
                        Material[] mats = r.materials;

                        foreach(Material m in mats)
                        {
                            //m.EnableKeyword("_EMISSION");
                            m.SetColor("_Color", Color.red);
                            if (m.IsKeywordEnabled("_EMISSION"))
                            {
                                emissionMats.Add(m);
                                m.SetColor("_EmissionColor", Color.red);
                            }
                            else
                            {
                                m.EnableKeyword("_EMISSION");
                                //m.SetColor("_EmissionColor", Color.red);
                            }
                            i++;
                     //       m.SetColor("_EmissionColor", Color.red);
                        }
                    }
                }
                Invoke("undoRed", 0.35f);
            }
            //Invoke("undoRed", 0.35f);

            return true;
        }
        else
        {
            return false;
        }
    }

    public void undoRed()
    {
        Renderer[] rens = gameObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer r in rens)
        {
            if (r.materials != null)
            {
                Material[] mats = r.materials;

                foreach (Material m in mats)
                {
                   m.SetColor("_Color", Color.white);
                   foreach (Material e in emissionMats)
                    {
                        if(m == e) //or m.Equals(e)
                        {
                            m.SetColor("_EmissionColor", Color.white);
                        }
                        else
                        {
                            m.DisableKeyword("_EMISSION");
                           // m.SetColor("_EmissionColor", Color.black);
                        }
                    }
                }
            }
        }
    }

    // Update is called once per frame
    protected virtual void UpdateBoss()
    {
        if(BossSource.volume < 1)
        {
            BossSource.volume = 1;
        }
        bossHealthBar.Val = EnemyHp;
        setBossHPText();

        //added by Clark
        if (Bosswave && !Bosskilled)
        {
            pos = waves.transform.position;
            pos.z += 0.0235f;
            waves.transform.position = pos;
        }

        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        SecondsinCurrentState += Time.deltaTime; //State Timer

        if (EnemyHp <= 0)
        {
            Death();
        }

        if(Currentstate == BossState.Dialog)
        {
            Dialog();
        }

        else if (Currentstate == BossState.Patrol) //All logic within patrol state
        {
            Patrol();
        }

        else if (Currentstate == BossState.PreparetoRush)
        {
            Prepare();
        }

        else if (Currentstate == BossState.Rush)
        {
            Special();
        }
    }

    public virtual void ResetSpawner()
    {
        BulletSpawner.SetActive(true);
        BulletSpawner.GetComponent<RadialBullet>().CanShoot = true;
        BulletSpawner.GetComponent<EnemyShots>().CanShoot = true;
        BulletSpawner.GetComponent<SpreadShot>().CanShoot = true;
    }

    public ParticleSystem GetCurrentParticleSystem()
    {
        if (CurrentShotParticleSystemIndex < ShotParticleSystems.Length)
        {
            return ShotParticleSystems[CurrentShotParticleSystemIndex];
        }

        return null;
    }

    public virtual void Dialog()
    {
        //do nothing
    }

    public virtual void Patrol()
    {
        //do nothing
    }

    public virtual void Prepare()
    {
        //do nothing
    }

    public virtual void Special()
    {
        //do nothing
    }

    public virtual void Death()
    {
        //do nothing
    }

    public virtual void playAnimation(String s)
    {
        //do nothing
    }

}

/* //Bullet system that isn't used

SecondsSinceLastSystemChange -= Time.deltaTime;
if (SecondsSinceLastSystemChange <= 0.0f)
{
    SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

    ParticleSystem previousSystem = GetCurrentParticleSystem();
    if (previousSystem != null)
    {
        previousSystem.Stop();
    }

    CurrentShotParticleSystemIndex++;
    if (CurrentShotParticleSystemIndex >= ShotParticleSystems.Length)
    {
        CurrentShotParticleSystemIndex = 0;
    }

    ParticleSystem nextSystem = GetCurrentParticleSystem();
    if (nextSystem != null)
    {
        nextSystem.Play();
    }
}
*/
