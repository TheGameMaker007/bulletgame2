﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroShipMovement : MonoBehaviour
{

    public float MovementSpeed = 4.0f;
    public int currentHealth;
    public GameObject PlayRoot;
    public GameObject HeroShip;

    public ParticleSystem slowParticles;
    //<<<<<<< HEAD
    //public GameObject FastTrain;
    //=======
    AudioSource audioData;
    //>>>>>>> da4d2dce02516771d78c5ce14f53fafec312c380

    public TimeManager timeManager; //For Timefreeze Mechanic
    public BombImgScript BombImgScript;

    int bombs = 0;
    public bool level1 = false;

    // Use this for initialization
    void Start()
    {
        if (level1)
        {
            if (PlayerPrefs.GetInt("PlayerBombs", 3) < 3)
            {
                PlayerPrefs.SetInt("PlayerBombs", 3);
            }
            if(PlayerPrefs.GetInt("PlayerLives", 5) < 5)
            {
                PlayerPrefs.SetInt("PlayerLives", 5);
            }
        }
        bombs = PlayerPrefs.GetInt("PlayerBombs", 3);

        Application.targetFrameRate = 60;
    }

    // Update is called once per frame
    void Update()
    {
        //bombs = PlayerPrefs.GetInt("PlayerBombs", 3);

        //Getting raw input bypasses Unity's input filtering which can mean quicker response to controls at the cost of having to ignore spurious input yourself
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");
        //float horizontalInput = Input.GetAxis("Horizontal");
        //float verticalInput = Input.GetAxis("Vertical");

        Vector3 desiredDirection = new Vector3(horizontalInput, 0.0f, verticalInput);
        desiredDirection.Normalize();

        transform.position += desiredDirection * Time.deltaTime * MovementSpeed;

        if (Input.GetKey(KeyCode.X) || Input.GetKeyDown(KeyCode.Joystick1Button4))
        {
            MovementSpeed = 1.5f;
            slowParticles.gameObject.SetActive(true);
            slowParticles.Play();
        }
        else if (Input.GetKeyUp(KeyCode.X) || Input.GetKeyUp(KeyCode.Joystick1Button4))
        {
            MovementSpeed = 4.0f;
            slowParticles.gameObject.SetActive(false);
            slowParticles.Stop();
        }

        if (Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.Joystick1Button5)) // ADD ORs For Controller
        {
            timeManager.DoSlowmotion();
        }
        else if (Input.GetKeyUp(KeyCode.C) || Input.GetKeyUp(KeyCode.Joystick1Button5))
        {
            timeManager.RegularMotion();
        }

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyUp(KeyCode.Joystick1Button2))
        {
            Invoke("SpaceAction", 0.01f);
        }

        Vector3 playerPositionAdjusted = transform.position;
        playerPositionAdjusted.x = Mathf.Clamp(playerPositionAdjusted.x, PlayRoot.transform.position.x - 4f, PlayRoot.transform.position.x + 5f);
        playerPositionAdjusted.z = Mathf.Clamp(playerPositionAdjusted.z, PlayRoot.transform.position.z - 4.54f, PlayRoot.transform.position.z + 4.54f);
        transform.position = playerPositionAdjusted;

        transform.position += desiredDirection * Time.deltaTime * MovementSpeed * (1.0f / Time.timeScale);
    }

    /*void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == FastTrain)
        {
            Debug.Log("FAST!");
            GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 10);
        }
    }*/

        void SpaceAction()
    {
        if (PlayerPrefs.GetInt("PlayerBombs") > 0)
        {
            bombs = PlayerPrefs.GetInt("PlayerBombs");
            removeBullets();
            bombs--;
            BombImgScript.useBomb();
            PlayerPrefs.SetInt("PlayerBombs", bombs);
        }
    }


    void removeBullets()
    {
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Bullet");

        foreach (GameObject bullet in prefabs)
        {
            Destroy(bullet);
        }
    }
}


/* public class damage : MonoBehaviour //damage set
 {
     void OnDamage() //setting damage
     {
         health--;
         if (health <= 0)
         {
             Destroy(gameObject);
         }
     }
 }
 */
