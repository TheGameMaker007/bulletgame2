﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss2 : BossEnemy
{
    // Start is called before the first frame update
    void Start()
    {
        base.StartBoss();
     //   setCurrentState(BossState.Dialog);
        bossAnimator.Play("Start Floating");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateBoss();
    }

    protected override void UpdateBoss()
    {
        bossHealthBar.Val = EnemyHp;
        setBossHPText();
        //added by Clark
        if (Bosswave && !Bosskilled)
        {
            if (!spawnMinions)
            {
               // pos = waves.transform.position;
                //pos.z += 0.0235f;
                //waves.transform.position = pos;
                pos = bossMinionsWaves.transform.position;
                pos.z += 0.014f;
                bossMinionsWaves.transform.position = pos;
            }
        }
        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        SecondsinCurrentState += Time.deltaTime; //State Timer

        if (EnemyHp <= 0)
        {
            Death();
        }

        if (Currentstate == BossState.Patrol) //All logic within patrol state
        {
            Patrol();
        }

        else if (Currentstate == BossState.PreparetoRush)
        {
            Prepare();
        }

        else if (Currentstate == BossState.Rush)
        {
            Special();
        }
    }

    public override void Patrol()
    {
        playAnimation("Idle");
        spawnMinions = false;
        if (player != null)
        {
            transform.LookAt(player.transform);
        }
        Vector3 MoveDirection = Bossmovement1Waypoints[waypointIndex].transform.position - transform.position;
        if (MoveDirection.magnitude < 0.5f)
        {
            waypointIndex++;
            if (waypointIndex >= Bossmovement1Waypoints.Length)
            {
                waypointIndex = 0;
            }
        }
        targetPoint = Bossmovement1Waypoints[waypointIndex].transform.localPosition; //Follows waypoints based on PlayZone
        transform.position = Vector3.MoveTowards(transform.position, targetPoint + PlayRoot.position, moveSpeed * Time.deltaTime);

        if (SecondsinCurrentState > 10f) //After 10 seconds, It prepares
        {
            spawnMinions = true;
            base.setCurrentState(BossState.PreparetoRush);
        }
    }


    public override void Dialog()
    {
        //BulletSpawner.SetActive(false);
        BulletSpawner.GetComponent<SpiralBullet>().CanShoot = false;
    }

    public override void Prepare()
    {
        if (player != null)
        {
            transform.LookAt(player.transform);
        }
        if (SecondsinCurrentState > .5f) //After 2 seconds, It rushes
        {
            setCurrentState(BossState.Rush);
            spawnMinions = true;
            //BulletSpawner.SetActive(false);
            //BulletSpawner.GetComponent<RadialBullet>().CanShoot = false;
            //BulletSpawner.GetComponent<EnemyShots>().CanShoot = false;
            BulletSpawner.GetComponent<SpiralBullet>().CanShoot = false;
        }
    }

    public override void ResetSpawner()
    {
        BulletSpawner.GetComponent<SpiralBullet>().CanShoot = true;
    }

    public override void Special()
    {

        if (player != null)
        {
            targetToFace = player.transform;

            float playerZ = 0.0f, ownZ = 0.0f;

            playerZ = targetToFace.position.z;
            ownZ = transform.position.z;

            
            /*
            if (ownZ < playerZ)
            {
                setCurrentState(BossState.Patrol); //Enemy passes Player
                Invoke("ResetSpawner", 3f);
            }
            else if (Damaged()) //Collision with player attackbox, send back to patrol
            {
                setCurrentState(BossState.Patrol); //Enemy passes Player
                Invoke("ResetSpawner", 3f);
            }
            
            else
            {
                transform.LookAt(targetToFace.position); //face player
                                                         //transform.Translate(velocity * Time.deltaTime); //MAKE ENEMY MOVE
                transform.position = Vector3.MoveTowards(transform.position, targetToFace.position, moveSpeed * Time.deltaTime);
            }
            */
        }
        else if (player == null)
        {
            transform.Translate(Vector3.back * Time.deltaTime);//LOGIC TO STOP GAME CRASH
            Invoke("ResetSpawner", 3f);
        }
        //transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, moveSpeed * Time.deltaTime);

        if (SecondsinCurrentState > 1.5f) //After 1.5 seconds
        {
            setCurrentState(BossState.Patrol);
            Invoke("ResetSpawner", 3f);
        }
    }

    public override void Death()
    {
        //AudioData.Play();
        bossBar.SetActive(false);
        // winscreen.SetActive(true);
        player.GetComponent<PlayerAttack>().boss = "3";
        player.GetComponent<PlayerHealthScript>().LifeCount++;
        BossSource.Stop();
        LevelSource.Play();
        Destroy(gameObject);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        BossDioEnd.SetActive(true);
    }

    public override void playAnimation(string s)
    {
        switch (s)
        {
            case "Idle":
                {
                    bossAnimator.SetTrigger(s);
                    break;
                }
            case "Charge":
                {
                    bossAnimator.ResetTrigger("Idle");
                    bossAnimator.ResetTrigger("Slash");
                    bossAnimator.ResetTrigger("Attack");
                    bossAnimator.SetTrigger(s);
                    //bossAnimator.Play("Start charging Attack");
                    break;
                }
            case "Slash":
                {
                    bossAnimator.ResetTrigger("Idle");
                    bossAnimator.ResetTrigger("Charge");
                    bossAnimator.ResetTrigger("Attack");
                    bossAnimator.SetTrigger(s);
                  //  bossAnimator.Play("Slash Arm");
                    break;
                }
            case "Attack": //shoot
                {
                    bossAnimator.ResetTrigger("Idle");
                    bossAnimator.ResetTrigger("Charge");
                    bossAnimator.ResetTrigger("Slash");
                    bossAnimator.SetTrigger(s);
                  //  bossAnimator.Play("Fire (Recoil)");
                    break;
                }
            case "Death":
                {
                    bossAnimator.Play("Death");
                    break;
                }
            default:
                break;
        }
    }
}
