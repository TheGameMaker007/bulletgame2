﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerAttack : MonoBehaviour
{
    private float timeBtwAttack;
    public float startTimeBtwAttack;
    public PlayerAttackBox AttackPrefab;
    public Animator animator;
    public int DelayAmount = 1;
    public GameObject bossSpawner;
    public GameObject AttackSpawn;

    public string boss;

    public ParticleSystem attackParticles;


    protected float Timer;

    void Update()
    {

        Timer += Time.deltaTime;

        if (Timer >= DelayAmount)
        {
            // then you can attack
            if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                Timer = 0f;
                animator.SetBool("Attack", true); //reference animation
                animator.Play("Attack"); //play animation
                attackParticles.gameObject.SetActive(true);
                attackParticles.Play();
                PlayerAttackBox spawnedAttack = GameObject.Instantiate<PlayerAttackBox>(AttackPrefab, transform.position, transform.rotation);
                spawnedAttack.Owner = AttackSpawn;
                spawnedAttack.boss = boss;
                Invoke("resetParticles", 1f);
            }

            timeBtwAttack = startTimeBtwAttack;

        }
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }

        animator.SetBool("Attack", false);
    }

    void resetParticles()
    {
        attackParticles.gameObject.SetActive(false);
        attackParticles.Stop();
    }
}
