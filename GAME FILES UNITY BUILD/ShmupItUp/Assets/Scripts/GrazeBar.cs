﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrazeBar : MonoBehaviour
{
    public float GrazeLevel;
    public Slider GrazeCountBar;

    // Start is called before the first frame update
    void Start()
    {
        GrazeLevel = 0;
    }

    //public void ToggleState()
   // {
        void OnParticleCollision(GameObject other)
        {
            GrazeLevel += .02f;
        }
    //}

    // Update is called once per frame
    void Update()
    {
        GrazeCountBar.value = GrazeLevel;
        
    }
}
