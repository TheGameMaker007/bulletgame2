﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderEnemyScript : MonoBehaviour
{
    public Transform targetToFace;
    public ParticleSystem[] ShotParticleSystems;
    public float SecondsBetweenShotSystemChanges = 5.0f;

    int CurrentShotParticleSystemIndex;
    float SecondsSinceLastSystemChange;
    public Vector3 velocity = new Vector3(1, 0, 0);

    private float latestDirectionChangeTime; //Movement
    private readonly float directionChangeTime = 3f;
    private float characterVelocity = 0.08f;
    private Vector3 movementDirection;
    private Vector3 movementPerSecond;

    private Vector3 position;
    private GameObject player;

    public bool bossSpider;
    public GameObject Boss;

    private int tempHp;

    // Use this for initialization
    void Start()
    {
        transform.Rotate(new Vector3(0, 90, 0));
        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        CurrentShotParticleSystemIndex = 0;
        SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

        tempHp = gameObject.GetComponent<EnemyHP>().EnemyHp;

        ParticleSystem nextSystem = GetCurrentParticleSystem();
        if (nextSystem != null)
        {
            nextSystem.Play();
        }

        latestDirectionChangeTime = 0f;
        calcuateNewMovementVector();
    }

    void calcuateNewMovementVector()
    {
        //create a random direction vector with the magnitude of 1, later multiply it with the velocity of the enemy
       // movementDirection = new Vector3(0, 0, -1).normalized;
       // movementPerSecond = movementDirection * characterVelocity;
        position = transform.position;
        position.Set(position.x, position.y, position.z - characterVelocity);

        //transform.LookAt(position);
        //transform.Rotate(new Vector3(0,90,0 )); SPEEN
        transform.position = position;
    }

    // Update is called once per frame
    void Update()
    {
        /* FOR CHASING PLAYER
        GameObject player = GameObject.FindGameObjectWithTag("Player"); //finding player
        if (player != null)
        {
            targetToFace = player.transform;

            float playerZ = 0.0f, ownZ = 0.0f;

            playerZ = targetToFace.position.z;
            ownZ = transform.position.z;

            if (ownZ < playerZ)
            {
                transform.Translate(Vector3.back * Time.deltaTime); //Enemy passes Player
            }
            else
            {
                transform.LookAt(targetToFace); //face player
                transform.Translate(velocity * Time.deltaTime); //MAKE ENEMY MOVE
            }
        }
        else if (player = null)
        {
            transform.Translate(Vector3.back * Time.deltaTime);//LOGIC TO STOP GAME CRASH
        }
        */

        // if (Time.time - latestDirectionChangeTime > directionChangeTime)
        // {
        //    latestDirectionChangeTime = Time.time;

        if (!PauseMenu.GameIsPaused)
        {
            if (bossSpider)
            {
                if (!Damaged())
                {
                    calcuateNewMovementVector();
                }
                // }

                if (Boss != null)
                {
                    if (transform.position.z < Boss.transform.position.z - 10)
                    {
                        Boss.GetComponent<MiniBoss1>().numBombs--;
                        //Boss.GetComponent<MiniBoss1>().bombs.Remove(gameObject);
                        Destroy(gameObject);
                    }

                    if (Damaged())
                    {
                        Vector3 targetPoint = Boss.transform.position; //Follows waypoints based on PlayZone
                        gameObject.GetComponent<SphereCollider>().enabled = false;
                        transform.Rotate(new Vector3(0, 15, 0)); //SPEEN
                        transform.position = Vector3.MoveTowards(transform.position, targetPoint, 3f * Time.deltaTime);

                        if (Vector3.Distance(transform.position, Boss.transform.position) <= 1f)
                        {
                            Boss.GetComponent<MiniBoss1>().EnemyHp -= 3;
                            //Boss.GetComponent<MiniBoss1>().numBombs--;
                            Destroy(gameObject);
                        }
                    }
                }
                else
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                calcuateNewMovementVector();
                if (player != null)
                {

                    if (transform.position.z < player.transform.position.z - 10)
                    {
                        Destroy(gameObject);
                    }
                }
            }

            // transform.position = new Vector3(transform.position.x + (movementPerSecond.x * Time.deltaTime),
            //  transform.position.z + (movementPerSecond.z * Time.deltaTime));

            SecondsSinceLastSystemChange -= Time.deltaTime;
            if (SecondsSinceLastSystemChange <= 0.0f)
            {
                SecondsSinceLastSystemChange = SecondsBetweenShotSystemChanges;

                ParticleSystem previousSystem = GetCurrentParticleSystem();
                if (previousSystem != null)
                {
                    previousSystem.Stop();
                }

                CurrentShotParticleSystemIndex++;
                if (CurrentShotParticleSystemIndex >= ShotParticleSystems.Length)
                {
                    CurrentShotParticleSystemIndex = 0;
                }

                ParticleSystem nextSystem = GetCurrentParticleSystem();
                if (nextSystem != null)
                {
                    nextSystem.Play();
                }
            }
        }
    }

    public bool Damaged()
    {
        if (tempHp > gameObject.GetComponent<EnemyHP>().EnemyHp)
        {
           // tempHp = gameObject.GetComponent<EnemyHP>().EnemyHp;
            return true;
        }
        else
        {
            return false;
        }
    }

    ParticleSystem GetCurrentParticleSystem()
    {
        if (CurrentShotParticleSystemIndex < ShotParticleSystems.Length)
        {
            return ShotParticleSystems[CurrentShotParticleSystemIndex];
        }

        return null;
    }
}
