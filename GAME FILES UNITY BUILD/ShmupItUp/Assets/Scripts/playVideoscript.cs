﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class playVideoscript : MonoBehaviour
{
    public VideoPlayer myVideo;
    public GameObject mainCanvas;
    public AudioSource levelSource;

    public bool outro = false;
    private int firstTime = 0;
   
    // Start is called before the first frame update
    void Start()
    {
        firstTime = PlayerPrefs.GetInt("SkipIntro", 0);
        if(firstTime == 0 || outro)
        {
            PlayerPrefs.SetInt("SkipIntro", 1);
            levelSource.Stop();
            myVideo = gameObject.GetComponent<VideoPlayer>();
            myVideo.Play();
        }
        else
        {
            mainCanvas.gameObject.SetActive(true);
            myVideo.Stop();
            gameObject.SetActive(false);
            levelSource.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(myVideo != null)
        {
            if(myVideo.isPlaying)
            {
                if (Input.anyKeyDown && !outro)
                {
                    mainCanvas.gameObject.SetActive(true);
                    myVideo.Stop();
                    gameObject.SetActive(false);
                    levelSource.Play();
                }
            }
            else
            {
                if (outro)
                {
                    SceneManager.LoadScene(0);
                }
                else
                {
                    mainCanvas.gameObject.SetActive(true);
                    myVideo.Stop();
                    gameObject.SetActive(false);
                    levelSource.Play();
                }
            }
        }
    }
}
