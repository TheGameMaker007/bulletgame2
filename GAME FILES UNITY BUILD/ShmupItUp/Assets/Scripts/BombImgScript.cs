﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombImgScript : MonoBehaviour
{
    public GameObject[] bombImgs;

    private int numbombs = 0;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("startBombImg", 0.01f);
    }

    public void useBomb()
    {
        if (numbombs > 0)
        {
            numbombs--;
            bombImgs[numbombs].SetActive(false);
        }
    }

    public void gainBomb()
    {
        if (numbombs < 5)
        {
            bombImgs[numbombs].SetActive(true);
            numbombs++;
        }
    }

    void startBombImg()
    {
        numbombs = PlayerPrefs.GetInt("PlayerBombs");
        if(numbombs > 5)
        {
            numbombs = 5;
        }

        for (int i = 0; i < numbombs; i++)
        {
            bombImgs[i].SetActive(true);
        }
    }
}
