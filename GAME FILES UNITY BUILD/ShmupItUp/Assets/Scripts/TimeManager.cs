﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    public float slowdownFactor = 0.001f;
    public float slowdownLength = 10f;
    public Slider GrazeCountBar;
    public int slidervalint;
    public GameObject GrazeBar;
    public float sliderDecreaseRate = 0.001f;
    bool slowdownActive;
    public AudioSource levelAudio;
    AudioSource audioData;

    public BombImgScript bombImgScript;

    private void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        audioData = GetComponent<AudioSource>();
    }

    public void Update()
    {
        //Time.timeScale += (1f / slowdownLength) * Time.unscaledDeltaTime;   //Used to bring back to regular speed, adjust this later on
        //Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);

        if (slowdownActive)
        {
            Debug.Log("bang"); //happening too late

            Debug.Log("val"+GrazeCountBar.value);
            GrazeCountBar.value -= sliderDecreaseRate * Time.unscaledDeltaTime;
            GrazeCountBar.value = Mathf.Clamp(GrazeCountBar.value, 0.0f, 1.0f);

            //This returns timeScale to 1.0f as sliderVal is reduced. Multiplying (1.0f - slowdownFactor) by a value < 1.0f can reduce the rate of this return.
            Time.timeScale = slowdownFactor + (1.0f - slowdownFactor) * ((1.0f - GrazeCountBar.value) / slowdownLength);

            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            //levelAudio.pitch = GrazeCountBar.value;

            levelAudio.pitch = 0.02f + (1.0f - GrazeCountBar.value) * 2f * Time.timeScale;
            levelAudio.pitch -= 0.75f;
            if (levelAudio.pitch <= 0)
            {
                levelAudio.pitch = 0.11f;
            }
            audioData.pitch = levelAudio.pitch;

            if (GrazeCountBar.value <= 0.0f)
            {
                GrazeCountBar.value = 0.0f;
                slowdownActive = false;
                levelAudio.pitch = 1.0f;
                audioData.pitch = levelAudio.pitch;
                Time.timeScale = 1.0f;
                Time.fixedDeltaTime = 0.02f;
            }
        }
        else
        {
            if (GrazeCountBar.value >= 0.99f)
            {
                addBomb();
            }
        }
    }

    void addBomb()
    {
        if (PlayerPrefs.GetInt("PlayerBombs") <= 5)
        {
        GrazeCountBar.value = 0.1f;
        Time.timeScale = 1.0f;
        Time.fixedDeltaTime = 0.05f;

        bombImgScript.gainBomb();
            int nb = PlayerPrefs.GetInt("PlayerBombs");
            nb++;
        PlayerPrefs.SetInt("PlayerBombs", nb);
        Debug.Log(PlayerPrefs.GetInt("PlayerBombs").ToString());
        }
    }

    public void DoSlowmotion ()
    {
        if (!slowdownActive)
        {
            if (GrazeCountBar.value >= 0.01f)
            {
                audioData = GetComponent<AudioSource>();
                audioData.Play();
                slowdownActive = true;
                Time.timeScale = slowdownFactor;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                levelAudio.pitch = 0.02f + (1.0f - GrazeCountBar.value) * Time.timeScale;
                levelAudio.pitch -= 0.75f;
                if(levelAudio.pitch <= 0)
                {
                    levelAudio.pitch = 0.11f;
                }
                audioData.pitch = levelAudio.pitch;
            }
        }
    }

    public void RegularMotion()
    {
        levelAudio.pitch = 1.0f;
        audioData.pitch = levelAudio.pitch;
        slowdownActive = false;
        Time.timeScale = 1.0f;
        Time.fixedDeltaTime = 0.02f;
    }
}
