﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackBox : MonoBehaviour
{
    public EnemyBullets Reflecter;
    //public GameObject waveSpawner;

    public string boss;

    public GameObject Owner;
    //private object inflictdamage;
    void Start()
    {
        
        Destroy(gameObject, .10f);
    }

    private void Update()
    {
        
        if (Owner != null)
        {
            transform.position = Owner.transform.position; //This breaks the game if PlayerAttack Box is active when the player dies
        }
        else
        {
            Destroy(gameObject);
        }
    }

    BossEnemy getBoss(Collider other)
    {
        //waveSpawner = GameObject.FindGameObjectWithTag("Boss");
        BossEnemy bossEnemyHP;

        switch(boss)
        {
            case "1":
                {
                     bossEnemyHP = other.gameObject.GetComponent<Boss1>();
                    break;
                }
            case "m1":
                {
                    bossEnemyHP = other.gameObject.GetComponent<MiniBoss1>();
                    break;
                }
            case "2":
                {
                     bossEnemyHP = other.gameObject.GetComponent<Boss2>();
                    break;
                }
            case "3":
                {
                     bossEnemyHP = other.gameObject.GetComponent<Boss3>();
                    break;
                }
            default:
                {
                    bossEnemyHP = other.gameObject.GetComponent<BossEnemy>();
                    break;
                }
                
        }
        return bossEnemyHP;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != Owner)
        {
            Debug.Log("Triggeractive");

            if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "noncollisionenemy")
            {
                //use bullet code to inflict damage to "enemies"
                Debug.Log("Damage!"); ; //set console to display damage is being dealt

                EnemyHP thisEnemyHP = other.gameObject.GetComponent<EnemyHP>();
                BossEnemy bossEnemyHP = getBoss(other);
                if (thisEnemyHP != null)
                {
                    thisEnemyHP.EnemyHp -= 3;
                    ScoreScript.scoreValue += 100; //ADD VALUE TO CURRENT SCORE
                }
                if (bossEnemyHP != null)
                {
                    bossEnemyHP.EnemyHp -= 3;//maybe change for boss?
                    ScoreScript.scoreValue += 100; //ADD VALUE TO CURRENT SCORE //Maybe change for boss?
                }
            }

            if (other.gameObject.tag == "Bullet")
            {
                Debug.Log("Reflect!");
                Reflecter = GameObject.FindObjectOfType(typeof(EnemyBullets)) as EnemyBullets;
                Reflecter.DrawPredictedReflectionPattern(transform.position, Vector3.back, 3); //Make this call WORK
                Destroy(other);
            }
            
        }


    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        Debug.Log("Reflect!");
        switch (collisionInfo.gameObject.tag)
        {
            case "Bullet":
                Reflecter = GameObject.FindObjectOfType(typeof(EnemyBullets)) as EnemyBullets;
                Reflecter.DrawPredictedReflectionPattern(transform.position, Vector3.back, 3);
            break;                                 //This part of the code would be trying to pull the reflection logic from
        }                                    //the EnemyBullets script into the PlayerAttackBox, but I'm not sure how to call it.
    }


}
