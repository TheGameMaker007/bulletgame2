﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerLives : MonoBehaviour
{

    PlayerHealthScript MyPlayer;
    Text MyText;

    // Start is called before the first frame update
    void Start()
    {
        MyText = GetComponent<Text>();
        MyPlayer = GameObject.FindWithTag("Player").GetComponent<PlayerHealthScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (MyText != null && MyPlayer != null)
        {
            //MyText.text = MyPlayer.LifeCount.ToString(); //Convert String to int
            MyText.text = "" + MyPlayer.LifeCount;
            //MyText.text = MyPlayer.LifeCount;
        }
    }
}
