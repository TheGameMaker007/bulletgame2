﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour
{
    public GameObject waves;
    public GameObject currUI;
    public GameObject nextUI;
    private Vector3 pos;

    public GameObject canvas;

    public enum Tutorials
    {
        Start,
        Movement,
        Attack,
        Slow,
        Timeslow
    }
    public Tutorials currTutorial;

    void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("MainCanvas");
        canvas.GetComponent<PauseMenu>().tutorial = true;

        if(PlayerPrefs.GetInt("FirstTime", 1) == 0)
        {
            end();
            canvas = GameObject.FindGameObjectWithTag("Tutorial");
            canvas.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        pos = waves.transform.position;
        pos.z += 0.0235f;
        waves.transform.position = pos;

        switch(currTutorial)
        {
            case Tutorials.Start:
                {
                    Invoke("next", 2f);
                    break;
                }
            case Tutorials.Movement:
                {
                    tutorialMovement();
                    break;
                }
            case Tutorials.Attack:
                {
                    tutorialAttack();
                    break;
                }
            case Tutorials.Slow:
                {
                    tutorialSlow();
                    break;
                }
            case Tutorials.Timeslow:
                {
                    tutorialTimeSlow();
                    break;
                }
            default:
                {
                    //do nothing
                    break;
                }
        }

    }

    void next()
    {
        nextUI.gameObject.SetActive(true);
        currUI.gameObject.SetActive(false);
    }

    void tutorialMovement()
    {
        if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            Invoke("next", 2.0f);
        }
    }

    void end()
    {
        canvas = GameObject.FindGameObjectWithTag("MainCanvas");
        PlayerPrefs.SetInt("FirstTime", 0);
        canvas.GetComponent<PauseMenu>().tutorial = false;
        currUI.gameObject.SetActive(false); //last tutorial so it closes the whole panel
    }

    void tutorialAttack()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            Invoke("next", 2.0f);
        }
    }

    void tutorialSlow()
    {
        if (Input.GetKey(KeyCode.X))
        {
            Invoke("next", 2.0f);
        }
    }

    void tutorialTimeSlow()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerHealthScript>().GrazeCountBar.value = 1f;
        if (Input.GetKey(KeyCode.C))
        {
            Invoke("end", 0.1f);
        }
    }
}
