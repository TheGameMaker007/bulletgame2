﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class DialogueScript : MonoBehaviour
{
    public Text myText;
    private string[] myLines;
    private string myLine;
    private int currLine = 0;
    private int index = 0;
    private float randTime = 0.1f;

    public bool Action = false;
    private int actionCounter = 0;

    public string fileName;

    public GameObject player;
    public GameObject trainSpawner;
    public Transform PlayRoot;
    public AudioSource levelaudio;
    public GameObject dialogBox;

    public bool lastBox =false;

    public enum BossNum
    {
        Boss1,
        Tutorial,
        Boss2,
        Boss3
    }
    public BossNum boss;

    // Start is called before the first frame update
    void Awake()
    {
        findBossStart();
        string filePath = Application.streamingAssetsPath + "/Text/" + fileName + ".txt";
        myLines = File.ReadAllLines(filePath);

        if(myLines != null)
        {
            Invoke("printDialog", randTime);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Action || actionCounter != 2)
        {
            waitForAction();
        }
    }

    void printDialog()
    {
        //char[] line = "".ToCharArray();
        //int index = 0;
        //Debug.Log("GOT HERE");
     //   Debug.Log(myLines.Length);
        if (currLine < myLines.Length)
        {
            if (!Action)
            {
                if (index < myLines[currLine].ToCharArray().Length)
                {
                    myLine = string.Concat(myLine, myLines[currLine].ToCharArray()[index]);
                    myText.text = myLine;
                    index++;
                    randTime = Random.Range(0.01f, 0.08f);
                    Invoke("printDialog", randTime);
                }
                else
                {
                    actionCounter = 1;
                }
            }
            else
            {
                if(actionCounter != 2)
                {
                    myText.text = myLines[currLine];
                }
                else
                {
                    if(currLine == myLines.Length-1)
                    {
                        if (lastBox)
                        {
                            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);   
                        }
                        trainSpawner.SetActive(true);
                        levelaudio.volume /= 0.5f;
                        unFreeze();
                        dialogBox.SetActive(false);
                    }
                    else
                    {
                        Debug.Log(currLine);
                        currLine++;
                        index = 0;
                        myLine = "";
                        Action = false;
                        actionCounter = 0;
                        Invoke("printDialog", randTime);
                    }
                }
            }
        }
        /*
        for(int i = 0; i < myLines[currLine].ToCharArray().Length; i++)
        {
           // line[index] = c;
           // index++;
           //if(i % 62 == 0 && i != 0)
            //{
               // myLine = string.Concat(myLine, "\n");
            //}
            //else
            //{
                myLine = string.Concat(myLine, myLines[currLine].ToCharArray()[i]);
            //}
         //   Debug.Log("GOT HERE");
            Invoke("updateDialog", 0.1f);
        }
        currLine++;
        */
    }

    void waitForAction()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            Action = true;
            actionCounter++;
            Invoke("printDialog", 0.1f);
        }
    }

    void freeze()
    {
        findBossStart();

        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        PlayRoot = GameObject.FindGameObjectWithTag("PlayZone").transform; //calls playroo
        player.GetComponent<HeroShipMovement>().enabled = false;
        player.GetComponentInChildren<Animator>().enabled = false;


        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Check World");
        PlayRoot.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);

        //City
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);
        }

        //Train
        prefabs = GameObject.FindGameObjectsWithTag("World");
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 0);
        }
    }

    void unFreeze()
    {
        findBossEnd();

        player = GameObject.FindGameObjectWithTag("Player"); //finding player
        PlayRoot = GameObject.FindGameObjectWithTag("PlayZone").transform; //calls playroo
        player.GetComponent<HeroShipMovement>().enabled = true;
        player.GetComponentInChildren<Animator>().enabled = true;
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("Check World");
        PlayRoot.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, 2);

        //City
        foreach (GameObject cityPrefab in prefabs)
        {
            if (cityPrefab.GetComponent<PlayAreaMovement>() != null)
                cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, -8);
        }

        //Train
        prefabs = GameObject.FindGameObjectsWithTag("World");
        foreach (GameObject cityPrefab in prefabs)
        {
            cityPrefab.GetComponent<PlayAreaMovement>().MovementPerSecond = new Vector3(0, 0, -1);
        }
    }

    void findBossStart()
    {
        GameObject[] currEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        if (currEnemies != null)
        {
            foreach (GameObject currBoss in currEnemies)
            {
                switch (boss)
                {
                    case BossNum.Boss1:
                        {
                            if (currBoss.GetComponent<Boss1>() != null)
                            {
                                currBoss.GetComponent<Boss1>().setCurrentState(BossEnemy.BossState.Dialog);
                            }
                            break;
                        }
                    case BossNum.Boss2:
                        {
                            if (currBoss.GetComponent<Boss2>() != null)
                            {
                                currBoss.GetComponent<Boss2>().setCurrentState(BossEnemy.BossState.Dialog);
                            }
                            break;
                        }
                    case BossNum.Boss3:
                        {
                            if (currBoss.GetComponent<Boss3>() != null)
                            {
                                currBoss.GetComponent<Boss3>().setCurrentState(BossEnemy.BossState.Dialog);
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }
    }

    void findBossEnd()
    {
        GameObject[] currEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        Debug.Log("gotHere1");
        if(currEnemies != null)
        {
            foreach (GameObject currBoss in currEnemies)
            {
                Debug.Log("gotHere2");
                switch (boss)
                {
                    case BossNum.Boss1:
                        {
                            if (currBoss.GetComponent<Boss1>() != null)
                            {
                                Debug.Log("gotHere3");
                                currBoss.GetComponent<Boss1>().setCurrentState(BossEnemy.BossState.Patrol);
                                currBoss.GetComponent<Boss1>().ResetSpawner();
                            }
                            break;
                        }
                    case BossNum.Boss2:
                        {
                            if (currBoss.GetComponent<Boss2>() != null)
                            {
                                currBoss.GetComponent<Boss2>().setCurrentState(BossEnemy.BossState.Patrol);
                                currBoss.GetComponent<Boss2>().ResetSpawner();
                            }
                            break;
                        }
                    case BossNum.Boss3:
                        {
                            if (currBoss.GetComponent<Boss3>() != null)
                            {
                                currBoss.GetComponent<Boss3>().setCurrentState(BossEnemy.BossState.Patrol);
                                currBoss.GetComponent<Boss3>().ResetSpawner();
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }
    }
}
